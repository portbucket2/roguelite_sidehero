﻿namespace APSdk
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "APSdkConfiguretionInfo", menuName = APSdkConstant.NameOfSDK + "/APSdkConfiguretionInfo")]
    public class APSdkConfiguretionInfo : ScriptableObject
    {
        #region Public Variables

        public bool logAPSdkLog = true;

        [Space(5.0f)]
        public bool maxMediationDebugger = false;

        [Space(5.0f)]
        public bool trackFacebookEvent = true;
        public bool trackAdjustEvent = false;

        #endregion

    }
}

