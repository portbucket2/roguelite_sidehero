﻿namespace APSdk
{
    using UnityEngine;

    public static class APSdkConstant
    {
        public static Color LogColor = Color.cyan;
        public static Color LogWarningColor = Color.yellow;
        public static Color LogErrorColor = Color.red;

        public const string NameOfSDK = "APSdk";

        public const int EXECUTION_ORDER_FacebookWrapper= -1000;
        public const int EXECUTION_ORDER_AdjustWrapper  = -900;
        public const int EXECUTION_ORDER_AdsWrapper     = -200;
        public const int EXECUTION_ORDER_LionKitWrapper = -100;
        
    }
}

