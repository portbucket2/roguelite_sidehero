﻿namespace APSdk
{
    using UnityEngine;
    using LionStudios;


    [DefaultExecutionOrder(APSdkConstant.EXECUTION_ORDER_LionKitWrapper)]
    public class APLionKitWrapper : MonoBehaviour
    {
        #region Public Callback

        public static APLionKitWrapper Instance { get; private set; }

        public APRewardedAdController RewardedAd { get; private set; } = new APRewardedAdController();
        public APInterstitialAdController InterstitialAd { get; private set; } = new APInterstitialAdController();
        public APBannerAdController BannerAd { get; private set; } = new APBannerAdController();

        #endregion

        #region Private Variables

        private static APSdkConfiguretionInfo apSdkInfo;

        #endregion

        #region Configuretion

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void OnGameStart()
        {
            if (Instance == null)
            {
                GameObject newAPLionKitWrapper = new GameObject("APLionKitWrapper");
                Instance = newAPLionKitWrapper.AddComponent<APLionKitWrapper>();

                DontDestroyOnLoad(newAPLionKitWrapper);

                apSdkInfo = Resources.Load<APSdkConfiguretionInfo>("APSdkConfiguretionInfo");

                // do MaxSdk.OnSdkInitializedEvent subscription here (SDK params)
                MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguretion) =>
                {
#if UNITY_IOS || UNITY_IPHONE
                    if (MaxSdkUtils.CompareVersions(UnityEngine.iOS.Device.systemVersion, "14.5") != MaxSdkUtils.VersionComparisonResult.Lesser)
                    {
                        AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);
                        APSdkLogger.Log("iOS 14.5+ detected!! SetAdvertiserTrackingEnabled = true");
                    }
                    else
                    {
                        APSdkLogger.Log("iOS <14.5 detected!! Normal Mode");
                    }
#endif
                };

                LionKit.OnInitialized += () => {

                    APSdkLogger.Log("LionKit Initialized");

                    // do facebook init
                    APFacebookWrapper.Instance.Initialize();

                    // do adjust init
                    APAdjustWrapper.Instance.Initialize();

                    // do event subscription
                    if (apSdkInfo.trackFacebookEvent) {

                        Analytics.OnLogEvent += (gameEvent) =>
                        {
                            APFacebookWrapper.Instance.LogEvent(
                                    gameEvent.eventName,
                                    gameEvent.eventParams
                                );
                        };
                    }

                    if (apSdkInfo.trackAdjustEvent) {

                        Analytics.OnLogEventUA += (gameEvent) =>
                        {
                            APAdjustWrapper.Instance.LogEvent(
                                    gameEvent.eventName,
                                    gameEvent.eventParams
                                );
                        };
                    }
                };

                
            }
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void OnSceneLoaded() {

            if (apSdkInfo.maxMediationDebugger)
            {
                MaxSdk.ShowMediationDebugger();
                APSdkLogger.Log("Showing Mediation Debugger");
            }
        }

        #endregion

        

    }
}


