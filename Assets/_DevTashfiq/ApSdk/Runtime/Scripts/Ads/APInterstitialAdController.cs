﻿namespace APSdk
{
    using UnityEngine.Events;
    using LionStudios.Ads;

    public class APInterstitialAdController
    {
        //Approch2  :   LionKit

        #region Public Variables

        public bool IsInterstitialAdReady { get { return Interstitial.IsAdReady; } }

        #endregion

        #region Private Variables

        private ShowAdRequest _showInterstitialAdRequest;

        private UnityAction _OnAdClosed;
        private UnityAction _OnAdFailed;

        #endregion

        #region Public Callback

        public APInterstitialAdController() {

            _showInterstitialAdRequest = new ShowAdRequest();

            // Ad event callbacks
            _showInterstitialAdRequest.OnDisplayed += adUnitId =>
            {
                APSdkLogger.Log("Displayed InterstitialAd :: Ad Unit ID = " + adUnitId);
            };
            _showInterstitialAdRequest.OnClicked += adUnitId =>
            {
                APSdkLogger.Log("Clicked InterstitialAd :: Ad Unit ID = " + adUnitId);
            };
            _showInterstitialAdRequest.OnHidden += adUnitId =>
            {
                APSdkLogger.Log("Closed InterstitialAd :: Ad Unit ID = " + adUnitId);
                _OnAdClosed?.Invoke();

            };
            _showInterstitialAdRequest.OnFailedToDisplay += (adUnitId, error) =>
            {
                APSdkLogger.LogError("Failed To Display InterstitialAd :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
                _OnAdFailed?.Invoke();

            };
        }

        public void ShowInterstitialAd(UnityAction OnAdFailed = null, UnityAction OnAdClosed = null)
        {
            _OnAdFailed = OnAdFailed;
            _OnAdClosed = OnAdClosed;

            Interstitial.Show(_showInterstitialAdRequest);
        }

        #endregion

    }
}
