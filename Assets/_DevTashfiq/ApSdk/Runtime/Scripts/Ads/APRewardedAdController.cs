﻿

namespace APSdk
{
    using UnityEngine.Events;
    using LionStudios;
    using LionStudios.Ads;

    public class APRewardedAdController
    {
        //Approch2  :   LionKit

        #region Public Variables

        public bool IsRewardedAdReady { get { return RewardedAd.IsAdReady; } }

        #endregion

        #region Private Variables

        private ShowAdRequest _ShowRewardedAdRequest;

        private string _eventName;
        private string _paramName;
        private object _paramValue;

        private UnityAction _OnAdFinished;
        private UnityAction _OnAdFailed;
        private UnityAction _OnAdClosed;

        #endregion


        #region Public Callback

        public APRewardedAdController() {

            _ShowRewardedAdRequest = new ShowAdRequest();

            // Ad event callbacks
            _ShowRewardedAdRequest.OnDisplayed += adUnitId =>
            {
                APSdkLogger.Log("Displayed Rewarded Ad :: Ad Unit ID = " + adUnitId);
            };
            _ShowRewardedAdRequest.OnClicked += adUnitId =>
            {
                APSdkLogger.Log("Clicked Rewarded Ad :: Ad Unit ID = " + adUnitId);
            };
            _ShowRewardedAdRequest.OnHidden += adUnitId =>
            {
                APSdkLogger.Log("Closed Rewarded Ad :: Ad Unit ID = " + adUnitId);
                _OnAdClosed?.Invoke();
            };
            _ShowRewardedAdRequest.OnFailedToDisplay += (adUnitId, error) =>
            {
                APSdkLogger.LogError("Failed To Display Rewarded Ad :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
                _OnAdFailed?.Invoke();
            };
            _ShowRewardedAdRequest.OnReceivedReward += (adUnitId, reward) =>
            {
                APSdkLogger.Log("Received Reward :: Reward = " + reward + " :: Ad Unit ID = " + adUnitId);
                _OnAdFinished?.Invoke();

                Analytics.LogEvent(
                    _eventName,
                    _paramName,
                    _paramValue);
            };
        }

        public void ShowRewardedAd(
            string eventName,
            string paramName,
            object paramValue,
            UnityAction OnAdFinished,
            UnityAction OnAdFailed = null,
            UnityAction OnAdClosed = null)
        {
            _eventName = eventName;
            _paramName = paramName;
            _paramValue = paramValue;

            _OnAdFinished = OnAdFinished;
            _OnAdFailed = OnAdFailed;
            _OnAdClosed = OnAdClosed;

            RewardedAd.Show(_ShowRewardedAdRequest);
        }

        public void ShowRewardedAd(UnityAction OnAdFinished, UnityAction OnAdFailed = null, UnityAction OnAdClosed = null) {

            _OnAdFinished = OnAdFinished;
            _OnAdFailed = OnAdFailed;
            _OnAdClosed = OnAdClosed;

            RewardedAd.Show(_ShowRewardedAdRequest);
        }

        #endregion

    }
}



