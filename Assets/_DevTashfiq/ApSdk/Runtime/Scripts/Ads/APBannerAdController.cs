﻿namespace APSdk
{
    using LionStudios.Ads;

    public class APBannerAdController
    {
        //Approch2  :   LionKit

        #region Public Variables

        public bool IsBannerAdReady { get { return Banner.IsAdReady; } }

        #endregion

        #region Private Variables

        private ShowAdRequest _ShowBannerAdRequest;

        #endregion

        #region Public Callback

        public APBannerAdController() {

            _ShowBannerAdRequest = new ShowAdRequest();

            // Ad event callbacks
            _ShowBannerAdRequest.OnDisplayed += adUnitId =>
            {
                APSdkLogger.Log("Displayed BannerAd :: Ad Unit ID = " + adUnitId);
            };
            _ShowBannerAdRequest.OnClicked += adUnitId =>
            {
                APSdkLogger.Log("Clicked BannerAd :: Ad Unit ID = " + adUnitId);
            };
            _ShowBannerAdRequest.OnHidden += adUnitId =>
            {
                APSdkLogger.Log("Closed BannerAd :: Ad Unit ID = " + adUnitId);

            };
            _ShowBannerAdRequest.OnFailedToDisplay += (adUnitId, error) =>
            {
                APSdkLogger.LogError("Failed To Display BannerAd :: Error = " + error + " :: Ad Unit ID = " + adUnitId);
            };
        }

        public void ShowBannerAd(int playerLevel = 0)
        {

            _ShowBannerAdRequest.SetLevel(playerLevel);
            Banner.Show(_ShowBannerAdRequest);
        }

        public void HideBannerAd() {

            Banner.Hide();
        }

        #endregion
    }
}

