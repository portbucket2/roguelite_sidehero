﻿namespace APSdk
{
#if UNITY_EDITOR

    using UnityEngine;
    using UnityEditor;

    public class APSdkIntegrationManagerEditorWindow : EditorWindow
    {
        #region Private Variables

        private static EditorWindow _reference;

        #endregion

        #region Editor

        [MenuItem("APSdk/APSdk Integration Manager")]
        public static void Create() {

            _reference = GetWindow<APSdkIntegrationManagerEditorWindow>("APSdk Integration Manager",typeof(APSdkIntegrationManagerEditorWindow));
        }

        #endregion
    }

#endif

}

