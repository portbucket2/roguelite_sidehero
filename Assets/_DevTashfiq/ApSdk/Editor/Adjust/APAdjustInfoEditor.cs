﻿namespace APSdk
{
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(APAdjustInfo))]
    public class APAdjustInfoEditor : Editor
    {
        #region Private Variables

        private APAdjustInfo _reference;

        private SerializedProperty _showBasicInfo;
        private SerializedProperty _showAdvancedInfo;

        private SerializedProperty _appTokenForAndroid;
        private SerializedProperty _appTokenForIOS;

        private SerializedProperty _environment;

        private SerializedProperty _logLevel;

        private SerializedProperty _startDelay;
        private SerializedProperty _startManually;
        
        private SerializedProperty _eventBuffering;
        private SerializedProperty _sendInBackground;
        private SerializedProperty _launchDeferredDeeplink;

        #endregion

        #region CustomGUI

        private void BasicCustomGUI() {

            EditorGUILayout.BeginHorizontal(GUI.skin.box);
            {
                string basicLabel = "[" + (!_showBasicInfo.boolValue ? "+" : "-") + "] Basic";
                GUIContent basicLabelContent = new GUIContent(
                        basicLabel

                    );
                GUIStyle basicLabelStyle = new GUIStyle(EditorStyles.boldLabel);
                basicLabelStyle.alignment = TextAnchor.MiddleLeft;


                if (GUILayout.Button(basicLabelContent, basicLabelStyle, GUILayout.Width(EditorGUIUtility.currentViewWidth)))
                {
                    _showBasicInfo.boolValue = !_showBasicInfo.boolValue;
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                if (_showBasicInfo.boolValue)
                {

                    EditorGUI.indentLevel += 1;

                    EditorGUILayout.PropertyField(_appTokenForAndroid);
                    EditorGUILayout.PropertyField(_appTokenForIOS);
                    EditorGUILayout.PropertyField(_environment);

                    EditorGUI.indentLevel -= 1;
                }
            }
            EditorGUILayout.EndVertical();
        }

        private void AdvanceCustomGUI() {

            EditorGUILayout.BeginHorizontal(GUI.skin.box);
            {
                string advanceLabel = "[" + (!_showAdvancedInfo.boolValue ? "+" : "-") + "] Advance";
                GUIContent advanceLabelContent = new GUIContent(
                        advanceLabel

                    );
                GUIStyle advanceLabelStyle = new GUIStyle(EditorStyles.boldLabel);
                advanceLabelStyle.alignment = TextAnchor.MiddleLeft;


                if (GUILayout.Button(advanceLabelContent, advanceLabelStyle, GUILayout.Width(EditorGUIUtility.currentViewWidth)))
                {
                    _showAdvancedInfo.boolValue = !_showAdvancedInfo.boolValue;
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                if (_showAdvancedInfo.boolValue)
                {

                    EditorGUI.indentLevel += 1;

                    EditorGUILayout.PropertyField(_logLevel);

                    EditorGUILayout.Space();
                    EditorGUILayout.PropertyField(_startDelay);
                    EditorGUILayout.PropertyField(_startManually);

                    EditorGUILayout.Space();
                    EditorGUILayout.PropertyField(_eventBuffering);
                    EditorGUILayout.PropertyField(_sendInBackground);
                    EditorGUILayout.PropertyField(_launchDeferredDeeplink);

                    EditorGUI.indentLevel -= 1;
                }
            }
            EditorGUILayout.EndVertical();

        }

        #endregion

        #region Editor

        private void OnEnable()
        {
            _reference = (APAdjustInfo)target;

            if (_reference == null)
                return;

            _showBasicInfo = serializedObject.FindProperty("showBasicInfo");
            _showAdvancedInfo = serializedObject.FindProperty("showAdvancedInfo");

            _appTokenForAndroid = serializedObject.FindProperty("appTokenForAndroid");
            _appTokenForIOS = serializedObject.FindProperty("appTokenForIOS");

            _environment = serializedObject.FindProperty("environment");

            _logLevel = serializedObject.FindProperty("logLevel");
            _startManually = serializedObject.FindProperty("startManually");
            _startDelay = serializedObject.FindProperty("startDelay");
            _eventBuffering = serializedObject.FindProperty("eventBuffering");
            _sendInBackground = serializedObject.FindProperty("sendInBackground");
            _launchDeferredDeeplink = serializedObject.FindProperty("launchDeferredDeeplink");

        }

        public override void OnInspectorGUI()
        {
            APSdkEditorModule.ShowScriptReference(serializedObject);

            serializedObject.Update();

            BasicCustomGUI();

            AdvanceCustomGUI();

            serializedObject.ApplyModifiedProperties();
        }

        #endregion
    }
}

