﻿namespace APSdk
{
#if UNITY_EDITOR

    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(APLionKitWrapper))]
    public class APLionKitWrapperEditor : Editor
    {
        #region Private Variables

        private APLionKitWrapper _reference;

        #endregion

        #region Editor

        private void OnEnable()
        {
            _reference = (APLionKitWrapper)target;

            if (_reference == null)
                return;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.BeginHorizontal(GUI.skin.box);
            {
                if (GUILayout.Button("RewardedAd")) {
                    _reference.RewardedAd.ShowRewardedAd(()=> { });
                }

                if (GUILayout.Button("InterstitialAd"))
                {
                    _reference.InterstitialAd.ShowInterstitialAd();
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal(GUI.skin.box);
            {
                if (GUILayout.Button("Show Banner")) {

                    _reference.BannerAd.ShowBannerAd();
                }

                if (GUILayout.Button("HideBanner Banner"))
                {
                    _reference.BannerAd.HideBannerAd();
                }
            }
            EditorGUILayout.EndHorizontal();

            serializedObject.ApplyModifiedProperties();
        }

        #endregion
    }
#endif
}

