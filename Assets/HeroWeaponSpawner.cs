﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroWeaponSpawner : MonoBehaviour
{
    public GameObject[] weaponObjs;
    public int currentWeaponIndex;
    public GameObject weaponHolder;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        //InitiateWeaponSpawner(2);
        GameStatesControl.instance.actionLevelResult += LevelResultFun;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer > HeroWeaponSpawnerManager.instance.lifetime)
        {
            DestroyMeWithAnoteherSpawn();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<HeroWeaponSystem>() != null)
        {
            other.GetComponent<HeroWeaponSystem>().ChangeHeroWeaponTo((HeroWeapons)currentWeaponIndex);
            StopAllCoroutines();
            DestroyMe();
        }
    }
    public void InitiateWeaponSpawner(int i)
    {
        currentWeaponIndex = i;
        GameObject go = Instantiate(weaponObjs[currentWeaponIndex - 1], weaponHolder.transform.position, weaponHolder.transform.rotation);
        go.transform.SetParent(weaponHolder.transform);
        //Destroy(this.gameObject, lifetime);
        //StartCoroutine(DestroyDelayedWithSpawn(HeroWeaponSpawnerManager.instance.lifetime));
    }
    public void LevelResultFun()
    {
        //StopAllCoroutines();
        DestroyMe();
    }
    public void DestroyMeWithAnoteherSpawn()
    {
        HeroWeaponSpawnerManager.instance.SpawnWeaponSpawnerDelayed(HeroWeaponSpawnerManager.instance.interval);
        DestroyMe();
    }
    //IEnumerator DestroyDelayedWithSpawn(float d)
    //{
    //    yield return new WaitForSeconds(d);
    //    DestroyMeWithAnoteherSpawn();
    //}
    public void DestroyMe()
    {
        GameStatesControl.instance.actionLevelResult -= LevelResultFun;
        Destroy(this.gameObject);
    }
}
