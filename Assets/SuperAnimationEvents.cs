﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperAnimationEvents : MonoBehaviour
{
    public ParticleSystem superEarthParticle;
    public ParticleSystem firestormCenter;
    public ParticleSystem[] firestormSides;
    public ParticleSystem thunderStormSpark;
    public ParticleSystem thunderStormHit;
    public ParticleSystem[] thunderInHand;
    public ParticleSystem laserHandLeft;
    public ParticleSystem laserHandRight;
    public ParticleSystem laserImpactLeft;
    public ParticleSystem laserImpactRight;
    public bool superRunning;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (superRunning)
        {
            LaserLineShow();
        }
    }

    public void SuperAnimStartFun()
    {
        superRunning = true;
        int a = SuperPowersUnlockSystem.instance.superMoveCurrent.superIndex;
        switch (a)
        {
            case 0:
                {
                    
                    break;
                }
            case 1:
                {
                    StartLaser();
                    break;
                }
            case 2:
                {
                    StartSuperParticleFirestorm();
                    break;
                }
            case 3:
                {
                    StartSuperThunder();
                    break;
                }
        }
    }
    public void SuperAnimJustAfterStartFun()
    {
        superRunning = true;
        int a = SuperPowersUnlockSystem.instance.superMoveCurrent.superIndex;
        switch (a)
        {
            case 0:
                {

                    break;
                }
            case 1:
                {
                    
                    break;
                }
            case 2:
                {
                    firestormSides[0].Play();
                    firestormSides[1].Play();
                    break;
                }
            case 3:
                {
                    
                    break;
                }
        }
    }
    public void SuperAnimHitFun()
    {
        superRunning = true;
        int a = SuperPowersUnlockSystem.instance.superMoveCurrent.superIndex;
        switch (a)
        {
            case 0:
                {
                    superEarthParticle.Play();
                    break;
                }
            case 1:
                {
                    ShootLaserRay();
                    break;
                }
            case 2:
                {
                    StartSuperParticleFirestormSideMove();
                    break;
                }
            case 3:
                {
                    SuperThunderHit();
                    break;
                }
        }
    }
    public void SuperAnimEndFun()
    {
        superRunning = false;
        int a = SuperPowersUnlockSystem.instance.superMoveCurrent.superIndex;
        switch (a)
        {
            case 0:
                {
                    
                    break;
                }
            case 1:
                {
                    StopLaserRay();
                    break;
                }
            case 2:
                {
                    StopSuperParticleFirestormSideMove();
                    break;
                }
            case 3:
                {
                    StopThunderHit();
                    break;
                }
        }
    }
    void StartSuperParticleFirestorm()
    {
        firestormSides[0].GetComponent<Animator>().SetBool("fireStormMove", false);
        firestormSides[1].GetComponent<Animator>().SetBool("fireStormMove", false);
        firestormSides[0].Stop();
        firestormSides[1].Stop();
        firestormSides[0].Clear();
        firestormSides[1].Clear();

        firestormCenter.Play();

        //firestormSides[0].Play();
        //firestormSides[1].Play();
        
    }
    
    void StartSuperParticleFirestormSideMove()
    {
        //firestormCenter.Play();

        firestormSides[0].GetComponent<Animator>().SetBool("fireStormMove", true);
        firestormSides[1].GetComponent<Animator>().SetBool("fireStormMove", true);
    }
    void StopSuperParticleFirestormSideMove()
    {
        firestormSides[0].Stop();
        firestormSides[1].Stop();
        //firestormSides[0].Clear();
        //firestormSides[1].Clear();

        //firestormSides[0].GetComponent<Animator>().SetBool("fireStormMove", false);
        //firestormSides[1].GetComponent<Animator>().SetBool("fireStormMove", false);
    }
    void StartSuperThunder()
    {
        thunderStormSpark.Play();
        thunderInHand[0].Play();
        //thunderInHand[1].Play();

    }
    public void SuperThunderHit()
    {

        thunderStormHit.Play();
    }
    public void StopThunderHit()
    {
        thunderStormSpark.Stop();
        thunderInHand[0]. Stop();
        thunderStormHit.  Stop();
    }
    void StartLaser()
    {
        laserImpactLeft.GetComponent<Animator>().SetBool("fireStormMove", false);

        laserImpactRight.GetComponent<Animator>().SetBool("fireStormMove", false);
    }
    void ShootLaserRay()
    {
        //laserImpactLeft.GetComponent<Animator>().SetBool("fireStormMove", false);
        
        //laserImpactRight.GetComponent<Animator>().SetBool("fireStormMove", false);

        laserHandLeft.Stop();
        laserHandRight.Stop();
        laserImpactLeft.Stop();
        laserImpactLeft.GetComponent<Animator>().SetBool("fireStormMove", false);
        //thunderStormSpark.Play();
        //thunderInHand[0] .Play();
        //thunderInHand[1].Play();
        laserHandLeft.Play();
        laserHandRight.Play();
        laserImpactLeft.Play();
        laserImpactRight.Play();
        laserImpactLeft.GetComponent<Animator>().SetBool("fireStormMove", true);
        laserImpactLeft.GetComponent<LineRenderer>().enabled = true;
        laserImpactLeft.GetComponent<LineRenderer>().SetPosition(0, laserImpactLeft.transform.position);
        laserImpactLeft.GetComponent<LineRenderer>().SetPosition(1, laserHandLeft.transform.position);
        //laserImpactRight;
        laserImpactRight.GetComponent<Animator>().SetBool("fireStormMove", true);
        laserImpactRight.GetComponent<LineRenderer>().enabled = true;
        laserImpactRight.GetComponent<LineRenderer>().SetPosition(0, laserImpactLeft.transform.position);
        laserImpactRight.GetComponent<LineRenderer>().SetPosition(1, laserHandLeft.transform.position);

    }
    void LaserLineShow()
    {
        if (laserImpactLeft.GetComponent<LineRenderer>().enabled)
        {
            laserImpactLeft.GetComponent<LineRenderer>().SetPosition(0, laserImpactLeft.transform.position);
            laserImpactLeft.GetComponent<LineRenderer>().SetPosition(1, laserHandLeft.transform.position);
        }
        if (laserImpactRight.GetComponent<LineRenderer>().enabled)
        {
            laserImpactRight.GetComponent<LineRenderer>().SetPosition(0, laserImpactRight.transform.position);
            laserImpactRight.GetComponent<LineRenderer>().SetPosition(1, laserHandRight.transform.position);
        }

    }
    void StopLaserRay()
    {
        //thunderStormSpark.Play();
        //thunderInHand[0] .Play();
        //thunderInHand[1].Play();
        laserHandLeft.   Stop();
        laserHandRight.  Stop();
        laserImpactLeft. Stop();
        laserImpactRight.Stop();
        //laserHandLeft.   Clear();
        //laserHandRight.  Clear();
        //laserImpactLeft. Clear();
        //laserImpactRight.Clear();
        //laserImpactLeft.GetComponent<Animator>().SetBool("fireStormMove",false);
        laserImpactLeft.GetComponent<LineRenderer>().enabled = false;
        //laserImpactRight;
       // laserImpactRight.GetComponent<Animator>().SetBool("fireStormMove", false);
        laserImpactRight.GetComponent<LineRenderer>().enabled = false;

    }
}
