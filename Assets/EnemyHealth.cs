﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float maxHealth;
    [SerializeField]
    float currentHeath;
    public GameObject enemyInside;
    public Slider enemyHelathSlider;
    public bool isDead;
    EnemyAttackSystem enemyAttackSystem;
    public bool IsBoss;
    public int spawnIndex;
    public ParticleSystem enemyHitPaticle;
    // Start is called before the first frame update
    void Start()
    {
        InitialTasks();
        enemyAttackSystem = GetComponent<EnemyAttackSystem>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DamageHeathBy(float t)
    {
        currentHeath -= t;
        currentHeath = Mathf.Clamp(currentHeath, 0, maxHealth);
        if(currentHeath <= 0)
        {
            enemyHitPaticle.Play();
            if (isDead)
            {
                return;
            }
            KillEnemy();
        }
        else
        {
            enemyHitPaticle.Play();
            if (enemyAttackSystem.attackAnimPlaying)
            {

            }
            else
            {
                GetComponent<EnemyAnimControl>().GoGetHit();
                GetComponent<EnemyAttackSystem>().AttackAnimEnd();
            }
            
        }
        UpdateHeathUi();

        ReferenceMaster.instance.superMoveUiManage.FillSuperBy(ReferenceMaster.instance.superMoveUiManage.fillIncreaseOnHitApp);
    }
    public void UpdateHeathUi()
    {
        enemyHelathSlider.value = Mathf.Lerp(0, 100, currentHeath / maxHealth);
    }
    public void InitialTasks()
    {
        currentHeath = maxHealth;
        UpdateHeathUi(); 
    }
    public void KillEnemy()
    {
        //this.gameObject.AddComponent<Rigidbody>();
        //Rigidbody rb = GetComponent<Rigidbody>();
        //rb.AddForce(transform.forward * -100 + transform.up * 500 + Vector3.forward *100);
        //rb.AddTorque(transform.right * -200);
        GetComponent<BoxCollider>().enabled = false;

        GetComponentInChildren<RagdollManager>().EnableRagdoll();
        GetComponentInChildren<RagdollManager>().GetRagHitRb().AddForce(transform.forward * -10000 + transform.up * 10000 + Vector3.forward * 10000);
        GetComponentInChildren<RagdollManager>().GetRagHitRb().AddTorque(transform.right * -20000);
        
        Destroy(gameObject,2f);
        enemyHelathSlider.gameObject.SetActive(false);
        
        isDead = true;

        if (IsBoss)
        {
            EnemySpawner.instance.CountABossKilled(this.gameObject);
            CamFollow.instance.TargetChangeTo(gameObject);
        }
        else
        {
            EnemySpawner.instance.CountAnEnemyKilled(this.gameObject);
        }
        
        GetComponent<EnemyAttackSystem>().PauseAttackLoop();
        GetComponent<EnemyAttackSystem>().StopAllCoroutines();
        EnemySpawner.instance.actionEnemyDeath?.Invoke();
        switch (enemyAttackSystem.enemyType)
        {
            case EnemyType.Ordinary:
                {

                    break;
                }
            case EnemyType.Hammer:
                {
                    GetComponent<EnemyAttackSystem>(). hammerRedZone.SetActive(false);
                    break;
                }
            case EnemyType.Molotov:
                {
                    GetComponentInChildren<ExplosiveThrowable>().RedZoneClear();
                    break;
                }
            case EnemyType.Grenade:
                {
                    GetComponentInChildren<ExplosiveThrowable>().RedZoneClear();
                    break;
                }
            case EnemyType.Suriken:
                {
                    //GetComponentInChildren<WeaponThrower>().ThrowWeapon();
                    break;
                }
            case EnemyType.Machette:
                {

                    break;
                }
            case EnemyType.Dagger:
                {

                    break;
                }
            case EnemyType.Baseball:
                {
                    GetComponent<EnemyAttackSystem>().hammerRedZone.SetActive(false);
                    break;
                }
            case EnemyType.Crowbar:
                {
                    GetComponent<EnemyAttackSystem>().hammerRedZone.SetActive(false);
                    break;
                }
            case EnemyType.PickAxe:
                {
                    GetComponent<EnemyAttackSystem>().hammerRedZone.SetActive(false);
                    break;
                }
            case EnemyType.Knife:
                {

                    break;
                }
        }
    }
    public void FlipEnemyInside()
    {
        enemyInside.transform.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));
    }
}
