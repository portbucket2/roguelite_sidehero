﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemySpawner : MonoBehaviour
{
    public Transform leftSpawnerPoint;
    public Transform rightSpawnerPoint;
    //public GameObject enemyPrefab;
    //public int maxEnemyCount;
    public int enemyCountThisLevel;
    public int bossCountThisLevel;
    public float bossSpawnInterval;
    //public float enemySpawnIntervalCurrent;
    int leftRightFac;
    public static EnemySpawner instance;
    [SerializeField]
    int enemySpawnCount;
    bool pausedEnemySpawn;
    public Action actionEnemySpawn;
    public Action actionEnemyDeath;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator EnemySpawnCoroutine( int enemySpawnIndex)
    {

        float i = LevelValuesCurrent.instance.enemyAppearIntervals[enemySpawnIndex];
        FacingMode f = LevelValuesCurrent.instance.enemySpawnDirection[enemySpawnIndex];
        if(f == FacingMode.Right)
        {
            leftRightFac = 1;
        }
        else
        {
            leftRightFac = -1;
        }
        yield return new WaitForSeconds(i);
        if(leftRightFac == 1)
        {
            SpawnEnemyRight();
            //leftRightFac = -1;
        }
        else
        {
            SpawnEnemyLeft();
            //leftRightFac = 1;
        }
        //Debug.Log("Coroutine");
        //yield return new WaitForSeconds(enemySpawnInterval);
        //if  (LevelValuesCurrent.instance.enemyListCurrentLevel.Count > 0)
        //{
        //    StartCoroutine(EnemySpawnCoroutine(enemySpawnInterval));
        //}
    }
    public IEnumerator BossSpawnCoroutine()
    {
        yield return new WaitForSeconds(bossSpawnInterval );
        UiManage.instance.BossFightPanelDisAppear();
        SpawnBoss(leftSpawnerPoint, true);
    }
    public void SpawnEnemyRight()
    {
        SpawnEnemy(rightSpawnerPoint);
    }
    public void SpawnEnemyLeft()
    {
        SpawnEnemy(leftSpawnerPoint, true);
    }
    public void SpawnEnemy(Transform t , bool flip = false)
    {
        if(LevelValuesCurrent.instance.enemyListCurrentLevel.Count > 0)
        {
            GameObject go = Instantiate(LevelValuesCurrent.instance.enemyListCurrentLevel[0], t.position, t.rotation);
            LevelValuesCurrent.instance.enemyListCurrentLevel.RemoveAt(0);
            LevelValuesCurrent.instance.enemyAliveCurrentLevel.Add(go);

            go.GetComponent<EnemyHealth>().spawnIndex = enemySpawnCount;

            actionEnemySpawn?.Invoke();

            if (flip)
            {
                go.GetComponent<EnemyHealth>().FlipEnemyInside();
                go.GetComponent<EnemyAnimControl>().SetFacinfModeMulTo(1);
            }
            else
            {
                go.GetComponent<EnemyAnimControl>().SetFacinfModeMulTo(-1);
            }
            AttentionIndicatorControl.instance.CheckIndicator();
            if (LevelValuesCurrent.instance.pauseSpawn[enemySpawnCount])
            {
                PauseEnemySpawning();
                
            }
            //enemySpawnCount += 1;
            if (LevelValuesCurrent.instance.enemyListCurrentLevel.Count <= 0)
            {
                return;
            }
            enemySpawnCount += 1;

            if (pausedEnemySpawn)
            {
                return;
            }
            //if (LevelValuesCurrent.instance.enemyListCurrentLevel.Count > 1)
            //{
            //    enemySpawnIntervalCurrent = LevelValuesCurrent.instance.enemyAppearIntervals[enemySpawnCount];
            //}

            StartCoroutine(EnemySpawnCoroutine( enemySpawnCount));
        }
        else
        {
            StopAllCoroutines();
        }
        

    }
    public void SpawnBoss(Transform t, bool flip = false)
    {
        if (LevelValuesCurrent.instance.bossListCurrentLevel.Count > 0)
        {
            GameObject go = Instantiate(LevelValuesCurrent.instance.bossListCurrentLevel[0], t.position, t.rotation);
            LevelValuesCurrent.instance.bossListCurrentLevel.RemoveAt(0);
            LevelValuesCurrent.instance.bossAliveCurrentLevel.Add(go);

           


            if (flip)
            {
                go.GetComponent<EnemyHealth>().FlipEnemyInside();
                go.GetComponent<EnemyAnimControl>().SetFacinfModeMulTo(1);
            }
            else
            {
                go.GetComponent<EnemyAnimControl>().SetFacinfModeMulTo(-1);
            }

            CamFollow.instance.TargetChangeTo(go);
        }
        else
        {
            StopAllCoroutines();
        }

    }
    public void StartEnemySpawning()
    {
        enemySpawnCount = 0;
        StartCoroutine(EnemySpawnCoroutine(enemySpawnCount));
    }

    public void CountAnEnemyKilled(GameObject e)
    {
        LevelValuesCurrent.instance.enemyAliveCurrentLevel.Remove(e);
        enemyCountThisLevel -= 1;

        if(enemyCountThisLevel <= 0)
        {
            if (LevelValuesCurrent.instance.hasBossFight)
            {
                UiManage.instance.actionUiDelayed = null;
                UiManage.instance.actionUiDelayed += UiManage.instance.BossFightPanelAppear;
                UiManage.instance.DelayedUiAppear(1);
                StartCoroutine(BossSpawnCoroutine());
            }
            else
            {
                LevelManager.instance.ChangeLevelResultTypeTo(LevelResultType.Success);
                GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult, 1);
            }
            
        }
        else
        {
            if(LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count <= 0)
            {
                if (pausedEnemySpawn)
                {
                    ResumeEnemySpawning();
                    StartCoroutine(EnemySpawnCoroutine(enemySpawnCount));
                }
            }
        }
    }
    public void CountABossKilled(GameObject e)
    {
        LevelValuesCurrent.instance.bossAliveCurrentLevel.Remove(e);
        bossCountThisLevel -= 1;

        if (bossCountThisLevel <= 0)
        {
            LevelManager.instance.ChangeLevelResultTypeTo(LevelResultType.Success);
            GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult, 1);

        }
    }
    public void StopAllEnemiesAttack()
    {
        for (int i = 0; i < LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count; i++)
        {
            LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyAttackSystem>().PauseAttackLoop();
        }
        for (int i = 0; i < LevelValuesCurrent.instance.bossAliveCurrentLevel.Count; i++)
        {
            LevelValuesCurrent.instance.bossAliveCurrentLevel[i].GetComponent<EnemyAttackSystem>().PauseAttackLoop();
        }
    }
    public void ReviveAllEnemiesAttack()
    {
        for (int i = 0; i < LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count; i++)
        {
            LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyMover>().GameRevivingStartForEnemy();
        }
        for (int i = 0; i < LevelValuesCurrent.instance.bossAliveCurrentLevel.Count; i++)
        {
            LevelValuesCurrent.instance.bossAliveCurrentLevel[i].GetComponent<EnemyMover>().GameRevivingStartForEnemy();
        }
    }
    IEnumerator EnemyDamageCoroutine(EnemyHealth e, float delay, float frac = 1)
    {
        yield return new WaitForSeconds(delay);
        e.DamageHeathBy(e.maxHealth*frac);

    }
    public void SuperMoveDamage()
    {
        //List<GameObject> enemyL = new List<GameObject>(LevelValuesCurrent.instance.enemyAliveCurrentLevel);
        //float d = 0;
        for (int i = 0; i < LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count; i++)
        {
            float dis = Vector3.Distance(ReferenceMaster.instance.heroHealth.gameObject.transform.position, LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].gameObject.transform.position);
            LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyMover>().enabled = false;
            StartCoroutine(EnemyDamageCoroutine(LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyHealth>(), dis*0.1f));
        }

        //while (LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count > 0)
        //{
        //    LevelValuesCurrent.instance.enemyAliveCurrentLevel[0].GetComponent<EnemyHealth>().DamageHeathBy(LevelValuesCurrent.instance.enemyAliveCurrentLevel[0].GetComponent<EnemyHealth>().maxHealth);
        //}
        List<GameObject> bosses = new List<GameObject>();
        //if(LevelValuesCurrent.instance.bossAliveCurrentLevel.Count > 0)
        //{
        //    bosses[0].GetComponent<EnemyHealth>().DamageHeathBy(LevelValuesCurrent.instance.bossAliveCurrentLevel[i].GetComponent<EnemyHealth>().maxHealth * 0.5f);
        //}
        //while (LevelValuesCurrent.instance.bossAliveCurrentLevel.Count > 0)
        //{
        //    LevelValuesCurrent.instance.bossAliveCurrentLevel[0].GetComponent<EnemyHealth>().DamageHeathBy((LevelValuesCurrent.instance.bossAliveCurrentLevel[0].GetComponent<EnemyHealth>().maxHealth)* 0.5f);
        //}
        //for (int i = 0; i < LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count; i++)
        //{
        //    Debug.Log(i);
        //    LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyHealth>().DamageHeathBy(LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyHealth>().maxHealth);
        //}
        for (int i = 0; i < LevelValuesCurrent.instance.bossAliveCurrentLevel.Count; i++)
        {
            bosses.Add(LevelValuesCurrent.instance.bossAliveCurrentLevel[i]);
            
        }
        for (int i = 0; i < bosses.Count; i++)
        {

            //bosses[i].GetComponent<EnemyHealth>().DamageHeathBy(bosses[i].GetComponent<EnemyHealth>().maxHealth * 0.5f);
            float dis = Vector3.Distance(ReferenceMaster.instance.heroHealth.gameObject.transform.position, bosses[i].transform.position);
            //LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyMover>().enabled = false;
            StartCoroutine(EnemyDamageCoroutine(bosses[i].GetComponent<EnemyHealth>(), dis * 0.1f,0.5f));
        }
    }

    public void PauseEnemySpawning()
    {
        pausedEnemySpawn = true;
    }
    public void ResumeEnemySpawning()
    {
        pausedEnemySpawn = false;
    }
    public bool EnemySpawnPausedOrNot()
    {
        return pausedEnemySpawn;
    }
    public void InitialTasks()
    {
        //enemySpawnCount = 0;
    }
    public int GetEnemySpawnCount()
    {
        return enemySpawnCount;
    }


}
