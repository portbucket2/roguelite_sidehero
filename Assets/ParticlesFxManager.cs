﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesFxManager : MonoBehaviour
{
    public TrailRenderer[] trails;
    public static ParticlesFxManager instance;
    public TrailRenderer[] weaponTrails;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DisableAllTrails();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DisableAllTrails()
    {
        for (int i = 0; i < trails.Length; i++)
        {
            trails[i].enabled = false;
        }
    }
    public void EnableTrail(int ind, float FaceModeMul)
    {
        CancelInvoke("DisableAllTrails");
        if (FaceModeMul == -1)
        {
            int ii = ind;
            switch (ii)
            {
                case 0:
                    {
                        ii = 1;
                        break;
                    }
                case 1:
                    {
                        ii = 0;
                        break;
                    }
                case 2:
                    {
                        ii = 3;
                        break;
                    }
            }

            ind = ii;
        }

        for (int i = 0; i < trails.Length; i++)
        {
            if (i == ind)
            {
                trails[i].enabled = true;
            }
            else
            {
                trails[i].enabled = false;
            }
            
        }

        Invoke("DisableAllTrails", 1f);
    }
    public void EnableWeaponTrail()
    {
        for (int i = 0; i < weaponTrails.Length; i++)
        {
            weaponTrails[i].enabled = true;
        }
    }
    public void DisableWeaponTrail()
    {
        for (int i = 0; i < weaponTrails.Length; i++)
        {
            weaponTrails[i].enabled =false;
        }
    }
}
