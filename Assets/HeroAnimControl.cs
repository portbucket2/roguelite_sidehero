﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAnimControl : CharAnimControl
{
    //public Vector2 animBlendKnobPos;
    //public Vector2 animBlendKnobTarPos;
    
    //Animator heroAnim;
    // Vector2 animBlendKnobPos;
    // Vector2 animBlendKnobTarPos;
    //public float snapSpeed;
    //HeroMover heroMover;
    //int attackAnimIndex;
    //float faceModeMul { get { return heroMover.GetFaceModeMul(); } }
    //// Start is called before the first frame update
    void Start()
    {
        charAnim = GetComponentInChildren<Animator>();
        heroMover = GetComponent<HeroMover>();
        heroMover.actionFaceSwap += AssignFaceModeMulToAnim;
        AssignFaceModeMulToAnim();
        GoIdle();
        
        
    }
    public void AssignFaceModeMulToAnim()
    {
        faceModeMul = heroMover.GetFaceModeMul();
    }
    private void Update()
    {
        if (animBlendKnobPos.x != animBlendKnobTarPos.x)
        {
            animBlendKnobPos.x = Mathf.MoveTowards(animBlendKnobPos.x, animBlendKnobTarPos.x, Time.deltaTime * snapSpeed);
            charAnim.SetFloat("blendX", animBlendKnobPos.x);
            //heroAnim.SetFloat("blendY", animBlendKnobPos.y);
        }
    }
    //
    //// Update is called once per frame
    //void Update()
    //{
    //    if (animBlendKnobPos.x != animBlendKnobTarPos.x)
    //    {
    //        animBlendKnobPos.x = Mathf.MoveTowards(animBlendKnobPos.x, animBlendKnobTarPos.x, Time.deltaTime * snapSpeed);
    //        heroAnim.SetFloat("blendX", animBlendKnobPos.x);
    //        //heroAnim.SetFloat("blendY", animBlendKnobPos.y);
    //    }
    //}
    //
    //public void GoIdle()
    //{
    //    
    //    animBlendKnobTarPos.x = 1.0f * faceModeMul;
    //    
    //}
    //public void GoPunchOne()
    //{
    //    animBlendKnobTarPos.x = 1f * faceModeMul;
    //    heroAnim.SetTrigger("punchOne");
    //}
    //public void GoPunchTwo()
    //{
    //    animBlendKnobTarPos.x = 1f * faceModeMul;
    //    heroAnim.SetTrigger("punchTwo");
    //}
    //public void GoKickOne()
    //{
    //    animBlendKnobTarPos.x = 1f * faceModeMul;
    //    heroAnim.SetTrigger("kickOne");
    //}
    //public void GoKickTwo()
    //{
    //    animBlendKnobTarPos.x = 1f * faceModeMul;
    //    heroAnim.SetTrigger("kickTwo");
    //}
    //public void GoGetHit()
    //{
    //    animBlendKnobTarPos.x = 1f * faceModeMul;
    //    heroAnim.SetTrigger("hit");
    //}
    //public void HeroAttackAnimPlay()
    //{
    //    int index = attackAnimIndex % 3;
    //    attackAnimIndex += 1;
    //    switch (index)
    //    {
    //        case 0:
    //            {
    //                GoPunchOne();
    //                break;
    //
    //            }
    //        case 1:
    //            {
    //                GoPunchTwo();
    //                break;
    //
    //            }
    //        case 2:
    //            {
    //                GoKickOne();
    //                break;
    //
    //            }
    //        case 3:
    //            {
    //                GoKickTwo();
    //                break;
    //
    //            }
    //        default:
    //            {
    //                GoPunchOne();
    //                break;
    //            }
    //    }
    //}
}
