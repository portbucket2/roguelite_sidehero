﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    public LevelResultType levelResultType;
    public bool shuffleLevels;
    public Action actionLevelLoadingDone;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LevelStartFun()
    {
        int levelIndex = GameManagementMain.instance.levelIndex;
        if (shuffleLevels)
        {
            levelIndex = GetComponent<LevelNumberRandomizer>().GetCurrentShuffleLevelOf(levelIndex);
        }
        LevelValuesCurrent.instance.InitialTasks(levelIndex);

        actionLevelLoadingDone?.Invoke();
    }
    public void ChangeLevelResultTypeTo(LevelResultType l)
    {
        levelResultType = l;
        switch (levelResultType)
        {
            case LevelResultType.Success:
                {
                    UiManage.instance.UiSuccessPanelsAppear();
                    GameManagementMain.instance.AnalyticsCallLevelAccomplished();
                    break;
                }
            case LevelResultType.Fail:
                {
                    UiManage.instance.UiFailedPanelsAppear();
                    GameManagementMain.instance.AnalyticsCallLevelFailed();
                    break;
                }
        }
    }
    public void LevelReset()
    {
        LevelValuesCurrent.instance.ResetIt();
        //ReferenceMaster.instance.heroHealth.HeroReset();
        ReferenceMaster.instance.heroHealth.HeroResetWithNoHeathRefill();
    }
}
