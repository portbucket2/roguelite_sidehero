﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndAreaClearedUi : MonoBehaviour
{
    public GameObject parentUi;
    public static LevelEndAreaClearedUi instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionGameStart += DisappearAreaClearedUi;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DisappearAreaClearedUi()
    {
        parentUi.SetActive(false);
    }
    public void AppearAreaClearedUi()
    {
        parentUi.SetActive(true);
    }
    
}
