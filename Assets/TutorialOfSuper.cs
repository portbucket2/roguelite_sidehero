﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialOfSuper : MonoBehaviour
{
    public int tutAppearLevel;
    public GameObject parent;
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionGamePlay += TutorialSuperAppearOrNot;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TutorialSuperAppearOrNot()
    {
        tutAppearLevel = SuperPowersUnlockSystem.instance.superMovesAvailable[0].unlockAtLevelIndex + 1;
        if (GameManagementMain.instance.levelIndexDisplayed == tutAppearLevel)
        {
            EnableTut();
            GetComponent<SuperMoveUiManage>(). SpecialSuperFillFull();
        }
        else
        {
            DisableTut();
        }
    }
    public void EnableTut()
    {
        parent.SetActive(true);
    }
    public void DisableTut()
    {
        parent.SetActive(false);
    }
    private void OnDisable()
    {
        DisableTut();
    }

}
