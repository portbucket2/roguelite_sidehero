﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using APSdk;

public class CardButton : MonoBehaviour
{
    public int cardIndex;
    public int abilityIndexChosen;
    public Text cardTitleText;
    public Image highlight;
    public Image watchAdImage;
    public Image cardImage;
    public Sprite[] cardContentSprites;
    public bool adClicked;
    public enum CardClickState
    {
        Clickable,Clicked,LockedForAd
    }
    public CardClickState cardState;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CardClickFun()
    {
        LevelEndCardsUi.instance.clickedButtonIndex = cardIndex;
        LevelEndCardsUi.instance.CardsGoAwayAfterSelection();
        LevelEndCardsUi.instance.AppearNextButtons();

        AbilitiesManager.instance.ApplyRightAbility(abilityIndexChosen);

        GameManagementMain.instance.AnalyticsCallAbilityChoose(cardTitleText.text);
    }
    public void CardInfoUpdate(int i)
    {
        AbilitiesManager.Ability ab = new AbilitiesManager.Ability();
        ab = AbilitiesManager.instance.abilities[i];

        cardTitleText.text = ab.title;

        if(cardContentSprites.Length > i)
        {
            cardImage.sprite = cardContentSprites[i];
        }
        

    }
    public void ChangeCardClickStateTo(CardClickState c)
    {
        if(c == cardState)
        {
            //return;
        }
        cardState = c;

        switch (cardState)
        {
            case CardClickState.Clickable:
                {
                    highlight.gameObject.SetActive(false);
                    watchAdImage.gameObject.SetActive(false);
                    GetComponent<Button>().enabled = true;
                    break;
                }
            case CardClickState.Clicked :
                {
                    highlight.gameObject.SetActive(true);
                    watchAdImage.gameObject.SetActive(false);
                    GetComponent<Button>().enabled = false;
                    break;
                }
            case CardClickState.LockedForAd:
                {
                    highlight.gameObject.SetActive(false);
                    watchAdImage.gameObject.SetActive(true);
                    GetComponent<Button>().enabled = false;
                    break;
                }
        }
    }

    public void WatchAdButtonFun()
    {
        //bool w = false;
        //if (w)
        //{
        //    AbilitiesManager.instance.ApplyRightAbility(abilityIndexChosen);
        //    UiManage.instance.NextButtonFun();
        //}
        //else
        //{
        //    UiManage.instance.NextButtonFun();
        //}
        int level = GameManagementMain.instance.levelIndexDisplayed + 1;
        //APSdkLogger.Log("(Before) AbilityCallForRevive = " + level);
        APLionKitWrapper.Instance.RewardedAd.ShowRewardedAd(
            "AbilityRV",
            "level",
            level,
            WatchAdSuccess,
            WatchAdFailed,
            WatchAdClosed);
    }
    public void WatchAdSuccess()
    {
        adClicked = true;
    }
    public void WatchAdFailed()
    {
        UiManage.instance.NextButtonFun();
    }
    public void WatchAdClosed()
    {
        if (adClicked)
        {
            AbilitiesManager.instance.ApplyRightAbility(abilityIndexChosen);
            GameManagementMain.instance.AnalyticsCallAbilityChoose(cardTitleText.text);
            UiManage.instance.NextButtonFun();
        }
        else
        {
            UiManage.instance.NextButtonFun();
        }
        adClicked = false;
    }
}
