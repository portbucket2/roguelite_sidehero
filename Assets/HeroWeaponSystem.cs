﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroWeaponSystem : MonoBehaviour
{
    public HeroWeapons heroWeaponCurrent;
    public float addedMoveStopRange;
    public float closeAttackRange;
    public GameObject weaponHolderRight;
    public GameObject weaponHolderLeft;
    public GameObject weaponCurrentObj;
    bool swapping;
    bool goingToHit;
    GameObject targetHolder;
    public Vector3 hitTargetPos;
    float hitTimer;
    public GameObject weaponHolder;
    public float throwSpeed;
    Vector3 oldPos;
    HeroMover heroMover;
    HeroAttackDodge heroAttackDodge;
    public GameObject[] weapons;
    public int hitCountOfCurrentWeapon;
    public int maxHitCount;
    [SerializeField]
    float addedDamagePercentageForWeapon;
    public float addedDamagePercentageForWeaponApp;
    public int unlockedWeaponCountSavedData;
    public int unlockedWeaponCountSavedDataForRevive;
    public int maxHitCountApp
    {
        get
        {
            return (int)((float)maxHitCount * AbilityCardMultipliers.instance.weaponLifetimeMul);
        }
    }
    public List<int> lockedWeapons;
    public List<int> unlockedWeapons;

    //public bool newLevel;
    
    // Start is called before the first frame update
    void Start()
    {
        heroAttackDodge = GetComponent<HeroAttackDodge>();
        heroMover = GetComponent<HeroMover>();

        heroMover.actionStartMovingToAttack += ThrowWeaponDecision;
        heroMover.actionStartMovingToAttack += WeaponSwapHand;

        ChangeHeroWeaponTo(HeroWeapons.None);
        heroAttackDodge.actionHeroDamageEnemy += HitCountOfWeaponUp;
        GameStatesControl.instance.actionLevelStart += LevelStartFun;
        //GetComponent<HeroMover>().actionGotEnemyWhileMoving += ThrowWeaponDecision;
        unlockedWeaponCountSavedData = PlayerPrefs.GetInt("unlockedWeaponCountSavedData", 0);
        PanelSavedProgress.instance.actionGameResume += UnlockWeaponsAccordingToSavedData;
        //AreaSelectionUiManage.instance.actionAreaStartButton += LockAllWeaponsAndSavedData;
        PanelSavedProgress.instance.actionNoGameResume += LockAllWeaponsAndSavedData;
        ReferenceMaster.instance.heroHealth.actionHeroDeath += LockAllWeaponsAndSavedData;
        LevelFailedCountdownSystem.instance.actionReviveHero += ReviveWeapon;

    }

    // Update is called once per frame
    void Update()
    {
        if (swapping)
        {
            if (goingToHit)
            {
                
            }

            weaponCurrentObj.transform.position = Vector3.MoveTowards(weaponCurrentObj.transform.position, targetHolder.transform.position, Time.deltaTime * 20);
            weaponCurrentObj.transform.rotation = Quaternion.RotateTowards(weaponCurrentObj.transform.rotation, targetHolder.transform.rotation, Time.deltaTime * 500);

            if(Vector3.Distance( weaponCurrentObj.transform.position , targetHolder.transform.position)<0.2f)
            {
                swapping = false;
                weaponCurrentObj.transform.position = targetHolder.transform.position;
                weaponCurrentObj.transform.SetParent(targetHolder.transform);
                weaponCurrentObj.transform.rotation = targetHolder.transform.rotation;

                ParticlesFxManager.instance.DisableWeaponTrail();
            }
        }

        if (goingToHit)
        {
            hitTimer += Time.deltaTime* throwSpeed;
            hitTimer = Mathf.Clamp(hitTimer, 0, 1);
            weaponCurrentObj.transform.position = Vector3.Lerp(oldPos, hitTargetPos + new Vector3(0,1,0), hitTimer);
            weaponCurrentObj.transform.rotation = Quaternion.LookRotation(hitTargetPos + new Vector3(0, 1, 0) - weaponCurrentObj.transform.position, transform.up);
            if (hitTimer >= 1)
            {
                hitTimer = 0;
                goingToHit = false;
                WeaponSwapHand();
            }
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            ChangeHeroWeaponTo(HeroWeapons.Shield);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            ChangeHeroWeaponTo(HeroWeapons.None);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            ChangeHeroWeaponTo(HeroWeapons.Hammer);
        }
    }
    public void WeaponSwapHand()
    {
        if(heroWeaponCurrent == HeroWeapons.None)
        {
            return;
        }

        if (goingToHit)
        {
            return;
        }

        swapping = true;
        weaponCurrentObj.transform.SetParent(weaponHolder.transform);
        float f = heroMover.GetFaceModeMul();
        if(f == 1)
        {
            targetHolder = weaponHolderRight;
        }
        else
        {
            targetHolder = weaponHolderLeft;
        }

    }
    public void ThrowWeapon()
    {
        goingToHit = true;
        swapping = false;
        weaponCurrentObj.transform.SetParent(weaponHolder.transform);
        oldPos = weaponCurrentObj.transform.position;
        
    }
    public void ThrowWeaponDecision()
    {
        if (heroWeaponCurrent == HeroWeapons.None)
        {
            return;
        }

        if (goingToHit)
        {
            return;
        }

        if (heroAttackDodge.enemyHealthInRange == null)
        {
            hitTargetPos = transform.position + transform.right * heroMover.GetFaceModeMul() * (addedMoveStopRange + 2);
            ThrowWeapon();
        }
        else
        {
            hitTargetPos = heroAttackDodge.enemyHealthInRange.gameObject.transform.position;
            float d = Vector3.Distance(transform.position, heroAttackDodge.enemyHealthInRange.gameObject.transform.position);
            if (d > closeAttackRange)
            {
                ThrowWeapon();
            }
        }
        
    }
    public void ChangeHeroWeaponTo(HeroWeapons w)
    {
        hitCountOfCurrentWeapon = 0;
        if (w == heroWeaponCurrent)
        {
            if((int)w != 0)
            {
                return;
            }
            
        }
        //hitCountOfCurrentWeapon = 0;
        heroWeaponCurrent = w;
        switch (heroWeaponCurrent)
        {
            case HeroWeapons.None:
                {
                    weapons[0].SetActive(false);
                    weapons[1].SetActive(false);

                    addedMoveStopRange = 0;

                    addedDamagePercentageForWeaponApp = 0;
                    break;
                }
            case HeroWeapons.Shield:
                {
                    EnableWeapon();
                    addedMoveStopRange = 2;
                    WeaponSwapHand();
                    addedDamagePercentageForWeaponApp = addedDamagePercentageForWeapon;
                    break;
                }
            case HeroWeapons.Hammer:
                {
                    EnableWeapon();

                    addedMoveStopRange = 2;
                    WeaponSwapHand();
                    addedDamagePercentageForWeaponApp = addedDamagePercentageForWeapon;
                    break;
                }
        }

        ReferenceMaster.instance.heroAnimControl.HeroWeaponIndexSetTo((int)heroWeaponCurrent);
    }
    public void EnableWeapon()
    {
        weapons[0].SetActive(false);
        weapons[1].SetActive(false);
        weaponCurrentObj = weapons[(int)heroWeaponCurrent -1];
        weaponCurrentObj.SetActive(true);
        weaponCurrentObj.transform.position = weaponHolderRight.transform.position;
        weaponCurrentObj.transform.SetParent(weaponHolderRight.transform);
    }
    public bool WeaponThrowOrNot()
    {
        bool r = false;
        if (heroAttackDodge.enemyHealthInRange == null)
        {
            hitTargetPos = transform.position + transform.right * heroMover.GetFaceModeMul() * (addedMoveStopRange + 2);
            r = true;
            //ThrowWeapon();
        }
        else
        {
            hitTargetPos = heroAttackDodge.enemyHealthInRange.gameObject.transform.position;
            float d = Vector3.Distance(transform.position, heroAttackDodge.enemyHealthInRange.gameObject.transform.position);
            if (d > closeAttackRange)
            {
                r = true;
                //ThrowWeapon();
            }
        }

        return r;
    }
    public void HitCountOfWeaponUp()
    {
        if(heroWeaponCurrent == HeroWeapons.None)
        {
            return;
        }
        hitCountOfCurrentWeapon += 1;
        if(hitCountOfCurrentWeapon > maxHitCountApp)
        {
            ChangeHeroWeaponTo(HeroWeapons.None);
            //if (newLevel)
            //{
            //      newLevel = false;
            //}
            //else
            //{
            //    HeroWeaponSpawnerManager.instance.SpawnWeaponSpawnerDelayed(HeroWeaponSpawnerManager.instance.interval);
            //}
            HeroWeaponSpawnerManager.instance.SpawnWeaponSpawnerDelayed(HeroWeaponSpawnerManager.instance.interval);
        }
    }
    public int GetLockedWeaponIndex()
    {
        int r = -1;
        if(lockedWeapons.Count > 0)
        {
            r = lockedWeapons[0];
        }
        return r;
    }
    public void UnlockWeapon(int i)
    {
        lockedWeapons.Remove(i);
        unlockedWeapons.Add(i);

        unlockedWeaponCountSavedData = unlockedWeapons.Count;
        PlayerPrefs.SetInt("unlockedWeaponCountSavedData", unlockedWeapons.Count);

    }
    public void LevelStartFun()
    {
        if(heroWeaponCurrent == HeroWeapons.None)
        {
            return;
        }
        //newLevel = true;
    }

    public void UnlockWeaponsAccordingToSavedData()
    {
        
        for (int i = 0; i < unlockedWeaponCountSavedData; i++)
        {
            lockedWeapons.Remove(i);
            unlockedWeapons.Add(i);
        }
        unlockedWeaponCountSavedData = unlockedWeapons.Count;
        PlayerPrefs.SetInt("unlockedWeaponCountSavedData", unlockedWeapons.Count);
    }
    void UnlockAllWeaponsAndSavedData()
    {
        while(lockedWeapons.Count > 0)
        {
            int x = lockedWeapons[0];
            lockedWeapons.Remove(lockedWeapons[0]);
            unlockedWeapons.Add(x);
        }
    }
    public void LockAllWeaponsAndSavedData()
    {
        UnlockAllWeaponsAndSavedData();
        while (unlockedWeapons.Count > 0)
        {
            int x = unlockedWeapons[0];
            unlockedWeapons.Remove(unlockedWeapons[0]);
            lockedWeapons.Add(x);
        }
        unlockedWeaponCountSavedDataForRevive = unlockedWeaponCountSavedData;
        unlockedWeaponCountSavedData = unlockedWeapons.Count;
        PlayerPrefs.SetInt("unlockedWeaponCountSavedData", unlockedWeapons.Count);

        
    }
    public void ReviveWeapon()
    {
        unlockedWeaponCountSavedData = unlockedWeaponCountSavedDataForRevive  ;
        UnlockWeaponsAccordingToSavedData();
    }

}
