﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimControl : CharAnimControl
{
    //public Animator charAnim;
    // Start is called before the first frame update
    EnemyAttackSystem enemyAttackSystem;
    
    void Start()
    {
        charAnim = GetComponentInChildren<Animator>();
        enemyAttackSystem = GetComponent<EnemyAttackSystem>();
        //GoIdle();
        GorRun();
        DetermineInitialBoolEnemyType(enemyAttackSystem.enemyType);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetFacinfModeMulTo(float f)
    {
        faceModeMul = f;
        //animBlendKnobTarPos.x = 1.0f * faceModeMul;
        //animBlendKnobPos.x = 1.0f * faceModeMul;
        charAnim = GetComponentInChildren<Animator>();
        charAnim.SetFloat("blendX", faceModeMul);
    }
    public void EnemyAttackAnimPlay()
    {
        EnemyType e = enemyAttackSystem.enemyType;
        switch (e)
        {
            case EnemyType.Ordinary:
                {
                    GoPunchEnemy();
                    break;
                }
            case EnemyType.Hammer:
                {
                    GoHammerHit();
                    break;
                }
            case EnemyType.Molotov:
                {
                    GoMolotovThrow();
                    break;
                }
            case EnemyType.Grenade:
                {
                    GoGranadeThrow();
                    break;
                }
            case EnemyType.Suriken:
                {
                    GoSurikenThrow();
                    break;
                }
            case EnemyType.Machette:
                {
                    GoMachetteAttack();
                    break;
                }
            case EnemyType.Dagger:
                {
                    GoDaggerAttack();
                    break;
                }
            case EnemyType.Baseball:
                {
                    GoBaseballAttack();
                    break;
                }
            case EnemyType.Crowbar:
                {
                    GoCrowbarAttack();
                    break;
                }
            case EnemyType.PickAxe:
                {
                    GoPickaxeAttack();
                    break;
                }
            case EnemyType.Knife:
                {
                    GoKnifeThrow();
                    break;
                }
            
        }
    }
    public void DetermineInitialBoolEnemyType(EnemyType e)
    {
        DisableAllAnimBools();
        switch (e)
        {
            case EnemyType.Ordinary:
                {
                    break;
                }
            case EnemyType.Hammer:
                {
                    charAnim.SetBool("HAMMER", true);
                    
                    break;
                }
            case EnemyType.Molotov:
                {
                    
                    charAnim.SetBool("MOLOTOV", true);
                    break;
                }
            case EnemyType.Grenade:
                {
                    charAnim.SetBool("GRANADE", true);
                    
                    break;
                }
            case EnemyType.Machette:
                {
                    charAnim.SetBool("MACHETTE", true);

                    break;
                }
            case EnemyType.Dagger:
                {
                    charAnim.SetBool("DAGGER", true);

                    break;
                }
            case EnemyType.Baseball:
                {
                    charAnim.SetBool("BASEBALL", true);

                    break;
                }
            case EnemyType.Crowbar:
                {
                    charAnim.SetBool("CROWBAR", true);

                    break;
                }
            case EnemyType.PickAxe:
                {
                    charAnim.SetBool("PICKAXE", true);

                    break;
                }
            case EnemyType.Knife:
                {
                    charAnim.SetBool("KNIFE", true);

                    break;
                }
            case EnemyType.Suriken:
                {
                    charAnim.SetBool("SURIKEN", true);

                    break;
                }
        }
    }
    public void DisableAllAnimBools()
    {
        charAnim.SetBool("HAMMER", false);
        charAnim.SetBool("GRANADE", false);
        charAnim.SetBool("MOLOTOV", false);
        charAnim.SetBool("MACHETTE", false);
        charAnim.SetBool("DAGGER",   false);
        charAnim.SetBool("BASEBALL", false);
        charAnim.SetBool("CROWBAR",  false);
        charAnim.SetBool("PICKAXE",  false);
        charAnim.SetBool("KNIFE",    false);
        charAnim.SetBool("SURIKEN", false);
    }
}
