﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuperMoveUnlockManager : MonoBehaviour
{
    public bool superMoveUnlocked;
    public float superMoveMoveUnlockPercentage;
    public float fillSpeed;
    //public static SuperMoveUnlockManager instance;
    public GameObject visuals;
    public Image imageFill;
    public Text textFill;
    public GameObject blockScreen;
    private void Awake()
    {
        //instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator percentageProgressFromTo( float to)
    {
        yield return null;
        if(superMoveMoveUnlockPercentage < to)
        {
            superMoveMoveUnlockPercentage += Time.deltaTime*fillSpeed;
            FillingFun();
            StartCoroutine(percentageProgressFromTo(to));
        }
        else
        {
            superMoveMoveUnlockPercentage = to;
            FillingFun();
            blockScreen.SetActive(false);
            if (superMoveMoveUnlockPercentage >= 100)
            {
                superMoveUnlocked = true;
                //visuals.SetActive(false);
                FilledFun();
            }
        }
        
    }
    public void FillingFun()
    {
        textFill.text = (int)superMoveMoveUnlockPercentage + " % unlocked";
        imageFill.fillAmount = superMoveMoveUnlockPercentage * 0.01f;
    }
    public void FilledFun()
    {
        textFill.text ="UNLOCKED";
        
    }
    public void SetProgressTo(float f)
    {
        StartCoroutine(percentageProgressFromTo(f));
    }
    public void SuperMoveUnlockLeveleEndFun()
    {
        if(LevelManager.instance.levelResultType == LevelResultType.Fail)
        {
            return;
        }
        if (superMoveUnlocked)
        {
            visuals.SetActive(false);
            return;
        }
        blockScreen.SetActive(true);
        int i = GameManagementMain.instance.levelIndexDisplayed;
        if(i == 0)
        {
            SetProgressTo(50);
        }
        else if(i == 1)
        {
            SetProgressTo(100);
        }
    }
    public void DetectUnlockAtMenuIdle()
    {
        int i = GameManagementMain.instance.levelIndexDisplayed;
        if (i >= 2)
        {
            superMoveUnlocked = true;
        }
    }

}
