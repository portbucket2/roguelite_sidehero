﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AbilitiesManager : MonoBehaviour
{
    public static AbilitiesManager instance;
    public List<Ability> abilities;
    

    [System.Serializable]
    public class Ability
    {
        public string title;
        public int abilityIndex;
        public Sprite icon;
        public Action actionAbility;
        public bool heathType;
    }

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ApplyRightAbility(int i)
    {
        Debug.Log(abilities[i].title);
        switch (i)
        {
            case 0:
                {
                    HealthMaxRangeIncrease(20);
                    break;
                }
            case 1:
                {
                    HealthValueIncreaseBy(40);
                    break;
                }
            case 2:
                {
                    InvincibilityOrbEnable();
                    break;
                }
            case 3:
                {
                    ArmourEnable();
                    break;
                }
            case 4:
                {
                    SlowDownEnemy();
                    break;
                }
            case 5:
                {
                    FasterSuperMoveRecharge(20);
                    break;
                }
            case 6:
                {
                    HeroHitDamageIncreaseBy(20);
                    break;
                }
            case 7:
                {
                    WeaponLifetimeIncreaseBy(20);
                    break;
                }
        }
    }
    public void HealthMaxRangeIncrease(float per)
    {
        AbilityCardMultipliers.instance.MaxHealthHeroMulIncreaseBy(per);
        ReferenceMaster.instance.heroHealth.HeroHealthIncreaseBy(per);
    }
    public void HealthValueIncreaseBy(float per)
    {
        ReferenceMaster.instance.heroHealth.HeroHealthIncreaseBy(per);
    }
    public void InvincibilityOrbEnable()
    {
        //ReferenceMaster.instance.heroHealth.GetComponent<HeroShieldSystem>().EnableShield();
        ReferenceMaster.instance.heroHealth.GetComponent<HeroShieldSystem>().ShieldPendindEnable();
    }
    public void ArmourEnable()
    {
        AbilityCardMultipliers.instance.HeroGetDamageMulDecreaseBy(20);
    }
    public void SlowDownEnemy()
    {
        AbilityCardMultipliers.instance.EnemyAttackIntervalMulIncreaseBy(20);
    }
    public void FasterSuperMoveRecharge(float per)
    {
        AbilityCardMultipliers.instance.SuperMoveRechargeMulIncreaseBy(per);
    }
    public void HeroHitDamageIncreaseBy(float per)
    {
        AbilityCardMultipliers.instance.HeroHitPowerMulIncreaseBy(per);
    }
    public void WeaponLifetimeIncreaseBy(float per)
    {
        AbilityCardMultipliers.instance.HeroWeaponLifetimeMulIncreaseBy(per);
    }
}
