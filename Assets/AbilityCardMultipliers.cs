﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityCardMultipliers : MonoBehaviour
{
    public static AbilityCardMultipliers instance;
    public float maxHealthHeroMul;
    public float heroGetDamageMul;
    public float enemyAttackIntervalMul;
    public float superMoveRechargeMul;
    public float heroHitPowerMul;
    public float weaponLifetimeMul;
    [Header("ReviveValues")]
    public float maxHealthHeroMulRevive;
    public float heroGetDamageMulRevive;
    public float enemyAttackIntervalMulRevive;
    public float superMoveRechargeMulRevive;
    public float heroHitPowerMulRevive;
    public float weaponLifetimeMulRevive;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        ReferenceMaster.instance.heroHealth.actionHeroDeath += ResetAllSavedValuesToDefault;
        //AreaSelectionUiManage.instance.actionAreaStartButton += ResetAllSavedValuesToDefault;
        PanelSavedProgress.instance.actionNoGameResume += ResetAllSavedValuesToDefault;
        LevelFailedCountdownSystem.instance.actionReviveHero += ReviveValuesRetrieve;
        GameStatesControl.instance.actionLevelStart += EvaluateResetAtLevelStart;

        maxHealthHeroMul       = PlayerPrefs.GetFloat("maxHealthHeroMul", 1);
        heroGetDamageMul       = PlayerPrefs.GetFloat("heroGetDamageMul", 1);
        enemyAttackIntervalMul = PlayerPrefs.GetFloat("enemyAttackIntervalMul", 1);
        superMoveRechargeMul   = PlayerPrefs.GetFloat("superMoveRechargeMul", 1);
        heroHitPowerMul        = PlayerPrefs.GetFloat("heroHitPowerMul", 1);
        weaponLifetimeMul      = PlayerPrefs.GetFloat("weaponLifetimeMul", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MaxHealthHeroMulIncreaseBy(float per)
    {
        maxHealthHeroMul *=(1+ (per * 0.01f));
        PlayerPrefs.SetFloat("maxHealthHeroMul", maxHealthHeroMul);
    }

    public void SuperMoveRechargeMulIncreaseBy(float per)
    {
        superMoveRechargeMul *= (1 + (per * 0.01f));
        PlayerPrefs.SetFloat("superMoveRechargeMul", superMoveRechargeMul);
    }
    public void HeroHitPowerMulIncreaseBy(float per)
    {
        heroHitPowerMul *= (1 + (per * 0.01f));
        PlayerPrefs.SetFloat("heroHitPowerMul", heroHitPowerMul);
    }
    public void HeroGetDamageMulDecreaseBy(float per)
    {
        heroGetDamageMul *= (1 - (per * 0.01f));
        PlayerPrefs.SetFloat("heroGetDamageMul", heroGetDamageMul);
    }
    public void EnemyAttackIntervalMulIncreaseBy(float per)
    {
        enemyAttackIntervalMul *= (1 + (per * 0.01f));
        PlayerPrefs.SetFloat("enemyAttackIntervalMul", enemyAttackIntervalMul);
    }
    public void HeroWeaponLifetimeMulIncreaseBy(float per)
    {
        weaponLifetimeMul *= (1 + (per * 0.01f));
        PlayerPrefs.SetFloat("weaponLifetimeMul", weaponLifetimeMul);
    }
    public void ResetAllSavedValuesToDefault()
    {
        ReviveValuesExtract();

        maxHealthHeroMul       =1;
        heroGetDamageMul       =1;
        enemyAttackIntervalMul =1;
        superMoveRechargeMul   =1;
        heroHitPowerMul        =1;
        weaponLifetimeMul = 1;

        PlayerPrefs.SetFloat("maxHealthHeroMul",      maxHealthHeroMul      );
        PlayerPrefs.SetFloat("heroGetDamageMul",      heroGetDamageMul      );
        PlayerPrefs.SetFloat("enemyAttackIntervalMul",enemyAttackIntervalMul);
        PlayerPrefs.SetFloat("superMoveRechargeMul",  superMoveRechargeMul  );
        PlayerPrefs.SetFloat("heroHitPowerMul",       heroHitPowerMul       );
        PlayerPrefs.SetFloat("weaponLifetimeMul", weaponLifetimeMul);
    }
    public void EvaluateResetAtLevelStart()
    {
        if(GameManagementMain.instance.levelIndex == 0)
        {
            ResetAllSavedValuesToDefault();
        }
    }

    public void ReviveValuesExtract()
    {
         maxHealthHeroMulRevive   =   maxHealthHeroMul      ;
         heroGetDamageMulRevive    =  heroGetDamageMul      ;
         enemyAttackIntervalMulRevive  =  enemyAttackIntervalMul;
         superMoveRechargeMulRevive  =  superMoveRechargeMul  ;
         heroHitPowerMulRevive  =  heroHitPowerMul       ;
         weaponLifetimeMulRevive =  weaponLifetimeMul     ;
    }
    public void ReviveValuesRetrieve()
    {
        maxHealthHeroMul =             maxHealthHeroMulRevive;
        heroGetDamageMul =             heroGetDamageMulRevive;
        enemyAttackIntervalMul = enemyAttackIntervalMulRevive;
        superMoveRechargeMul =     superMoveRechargeMulRevive;
        heroHitPowerMul =               heroHitPowerMulRevive;
        weaponLifetimeMul = weaponLifetimeMulRevive;

        PlayerPrefs.SetFloat("maxHealthHeroMul", maxHealthHeroMul);
        PlayerPrefs.SetFloat("heroGetDamageMul", heroGetDamageMul);
        PlayerPrefs.SetFloat("enemyAttackIntervalMul", enemyAttackIntervalMul);
        PlayerPrefs.SetFloat("superMoveRechargeMul", superMoveRechargeMul);
        PlayerPrefs.SetFloat("heroHitPowerMul", heroHitPowerMul);
        PlayerPrefs.SetFloat("weaponLifetimeMul", weaponLifetimeMul);
    }
}
