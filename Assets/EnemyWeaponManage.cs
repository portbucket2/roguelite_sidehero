﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponManage : MonoBehaviour
{
    public GameObject hammerRight;
    public GameObject hammerLeft;
    
    // Start is called before the first frame update
    void Start()
    {
        GetInitialWeapon(GetComponent<EnemyAnimControl>().faceModeMul);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetInitialWeapon(float faceModeMul )
    {
        FacingMode f = FacingMode.Right;
        if(faceModeMul == -1)
        {
            f = FacingMode.Left;
        }
        if(hammerLeft != null && hammerRight != null)
        {
            if (f == FacingMode.Right)
            {
                hammerRight.SetActive(true);
                hammerLeft.SetActive(false);
            }
            else
            {
                hammerRight.SetActive(false);
                hammerLeft.SetActive(true);
            }
        }
        return;
        //EnemyType e = GetComponent<EnemyAttackSystem>().enemyType;
        //switch (e)
        //{
        //    case EnemyType.Ordinary:
        //        {
        //            
        //            break;
        //        }
        //    case EnemyType.Hammer:
        //        {
        //           if(f == FacingMode.Right)
        //            {
        //                hammerRight.SetActive(true);
        //                hammerLeft.SetActive(false);
        //            }
        //            else
        //            {
        //                hammerRight.SetActive(false);
        //                hammerLeft.SetActive(true);
        //            }
        //            break;
        //        }
        //    case EnemyType.Molotov:
        //        {
        //            if (f == FacingMode.Right)
        //            {
        //                hammerRight.SetActive(true);
        //                hammerLeft.SetActive(false);
        //            }
        //            else
        //            {
        //                hammerRight.SetActive(false);
        //                hammerLeft.SetActive(true);
        //            }
        //            break;
        //        }
        //    case EnemyType.Grenade:
        //        {
        //            if (f == FacingMode.Right)
        //            {
        //                hammerRight.SetActive(true);
        //                hammerLeft.SetActive(false);
        //            }
        //            else
        //            {
        //                hammerRight.SetActive(false);
        //                hammerLeft.SetActive(true);
        //            }
        //            break;
        //        }
        //}
    }
}
