﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeroMover : MonoBehaviour
{
    public string s;
    public FacingMode heroFacingModeCurrent;
    public float moveStepAttack;
    public float moveStepDodge;
    [SerializeField]
    float moveTimer;
    [SerializeField]
    float moveTime;
    [SerializeField]
    bool moving;
    [SerializeField]
    Vector3 targetPos;
    [SerializeField]
    Vector3 oldPos;
    [SerializeField]
    float faceModeMul = 1;
    [SerializeField]
    float inputModeMul = 1;
    [SerializeField]
    float movingDirectionMul = 1;

    [SerializeField]
    float moveStopRange = 1.5f;
    float moveStopRangeApp
    {
        get
        {
            return moveStopRange + GetComponent<HeroWeaponSystem>().addedMoveStopRange;
        }
    }
    AnimationCurve moveAnimCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public GameObject facingArrow;
    public LayerMask enemyLayer;
    public LayerMask throwableWeaponLayer;
    HeroAttackDodge heroAttackDodge;
    HeroHealth heroHealth;
    public Action actionFaceSwap;

    public GameObject enemyAtFront;
    public GameObject enemyAtBack;
    public Action actionStartMovingToAttack;
    public Action actionGotEnemyWhileMoving;
    //public EnemyHealth enemyHealthInRange;

    // Start is called before the first frame update
    void Start()
    {
        heroAttackDodge = GetComponent<HeroAttackDodge>();
        heroHealth = GetComponent<HeroHealth>();

        InputMouseSystem.instance.actionSwipeRight += InputAtRight;
        InputMouseSystem.instance.actionSwipeLeft += InputAtLeft;

        
        GameStatesControl.instance.actionGameStart += HeroRepositionToCenter;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            //ChangrFacingModeTo(FacingMode.Right);
            InputAtRight();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            //ChangrFacingModeTo(FacingMode.Left);
            InputAtLeft();


        }
        
        if (heroHealth.HeroDeadOrnot())
        {
            return;
        }
        MoveToTargetPos();
    }

    public void MoveToTargetPos()
    {
        if (!moving)
        {
            return;
        }
        if (moveTimer< 1)
        {
            //enemyHealthInRange = null;
            GameObject g = RaycastForThrwableWeapon(moveStopRangeApp, true);
            if(g != null && movingDirectionMul == 1)
            {
                StopMoving();
                heroAttackDodge.DisableHeroDamageability();
                g.GetComponent<WeaponThrowable>(). CollideMe();

                actionGotEnemyWhileMoving?.Invoke();
                Debug.Log(g.name);
                return;
            }
            GameObject go = RaycastForEnemy(moveStopRangeApp, true);
            if (go != null)
            {
                heroAttackDodge.enemyHealthInRange = go.GetComponent<EnemyHealth>();
                //Debug.Log(go.name);
                StopMoving();

                actionGotEnemyWhileMoving?.Invoke();
                return;
            }
            moveTimer += Time.deltaTime*(1/moveTime);
            if (moveTimer >= 1)
            {
                moveTimer = 1;
                StopMoving();
                
            }
            //transform.position = Vector3.Lerp(oldPos, targetPos, moveAnimCurve.Evaluate(moveTimer));
            transform.position = Vector3.Lerp(transform.position, Vector3.Lerp(oldPos, targetPos, moveTimer),Time.deltaTime*20f);
        }
       
        
    }
    public void StartMovingToAttack(float moveT = 1)
    {
        moving = true;
        movingDirectionMul = 1;

        heroAttackDodge.enemyHealthInRange = null;
        GameObject go = RaycastForEnemy(heroAttackDodge.heroAttackRange, true);
        if(go != null)
        {
            heroAttackDodge.enemyHealthInRange = go.GetComponent<EnemyHealth>();
            //Debug.Log(go.name);
        }
        

        
        oldPos = transform.position;
        targetPos = oldPos + new Vector3(moveStepAttack * faceModeMul, 0, 0);
        moveTimer = 0;
        moveTime = moveT;
        heroAttackDodge.HeroAttack();

        actionStartMovingToAttack?.Invoke();
    }
    public void StartMovingToDodge(float moveT = 1)
    {
        heroAttackDodge.enemyHealthInRange = null;

        moving = true;
        movingDirectionMul = -1;
        oldPos = transform.position;
        targetPos = oldPos + new Vector3(moveStepDodge * -faceModeMul, 0, 0);
        moveTimer = 0;
        moveTime = moveT;
        ReferenceMaster.instance.heroAnimControl.GoDodge();
    }
    public void StopMoving()
    {
        moving = false;
        

    }
    public void ChangrFacingModeTo(FacingMode m)
    {
        heroFacingModeCurrent = m;

        switch (heroFacingModeCurrent)
        {
            case (FacingMode.Right):
            {
                    faceModeMul = 1;
                    facingArrow.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    break;
            }
            case (FacingMode.Left):
                {
                    faceModeMul = -1;
                    facingArrow.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
                    break;
                }
        }
    }
    public void SwapFacingMode()
    {
        if(heroFacingModeCurrent == FacingMode.Right)
        {
            ChangrFacingModeTo(FacingMode.Left);

        }
        else if(heroFacingModeCurrent == FacingMode.Left)
        {
            ChangrFacingModeTo(FacingMode.Right);
        }
        actionFaceSwap?.Invoke();
        ReferenceMaster.instance.heroAnimControl.GoIdle();
        
    }
    public GameObject RaycastForEnemy(float distance, bool front = true, bool initialSearch = false)
    {
        RaycastHit hit;
        GameObject hitObj = null;
        float frontBackMul = 1;
        float movingDirMul = 1;
        if (!front)
        {
            frontBackMul = -1;
        }
        if (!initialSearch)
        {
            movingDirMul = movingDirectionMul;
        }
        // Does the ray intersect any objects excluding the player layer
        //if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.right * faceModeMul*frontBackMul * movingDirectionMul, out hit, distance,enemyLayer))
        //{
        //    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        //    //Debug.Log("Did Hit");
        //    hitObj =  hit.transform.gameObject;
        //}
        if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.right * faceModeMul * frontBackMul * movingDirMul, out hit, distance, enemyLayer))
        {
            //Debug.DrawRay(transform.position + new Vector3(0, 1, 0), transform.right * faceModeMul * frontBackMul * movingDirectionMul * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            hitObj = hit.transform.gameObject;
        }
        else
        {
           // Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
            //return null;
        }

        return hitObj;
    }
    public GameObject RaycastForThrwableWeapon(float distance, bool front = true, bool initialSearch = false)
    {
        RaycastHit hit;
        GameObject hitObj = null;
        float frontBackMul = 1;
        float movingDirMul = 1;
        if (!front)
        {
            frontBackMul = -1;
        }
        if (!initialSearch)
        {
            movingDirMul = movingDirectionMul;
        }
        // Does the ray intersect any objects excluding the player layer
        //if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.right * faceModeMul*frontBackMul * movingDirectionMul, out hit, distance,enemyLayer))
        //{
        //    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        //    //Debug.Log("Did Hit");
        //    hitObj =  hit.transform.gameObject;
        //}
        if (Physics.Raycast(transform.position + new Vector3(0, 1.5f, 0), transform.right * faceModeMul * frontBackMul * movingDirMul, out hit, distance, throwableWeaponLayer))
        {
            //Debug.DrawRay(transform.position + new Vector3(0, 1, 0), transform.right * faceModeMul * frontBackMul * movingDirectionMul * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            hitObj = hit.transform.gameObject;
        }
        else
        {
            // Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
            //return null;
        }

        return hitObj;
    }
    public void InputAtRight()
    {
        inputModeMul = 1;
        EvaluateInputAndTakeAction();

    }
    public void InputAtLeft()
    {
        inputModeMul = -1;
        EvaluateInputAndTakeAction();

    }
    public void EvaluateInputAndTakeAction()
    {
        if (heroHealth.HeroDeadOrnot())
        {
            return;
        }
        if (heroHealth.superMoveMode)
        {
            return;
        }
        enemyAtFront = null;
        enemyAtBack = null;
        if (inputModeMul == faceModeMul)
        {
            StartMovingToAttack(0.25f);
        }
        else{
            
            enemyAtFront = RaycastForEnemy(heroAttackDodge.heroAttackRange, true, true);
            enemyAtBack = RaycastForEnemy(heroAttackDodge.heroAttackRange, false, true);
            if (enemyAtBack != null)
            {
                SwapFacingMode();
                StartMovingToAttack(0.25f);
            }
            else if (enemyAtFront != null)
            {
                StartMovingToDodge(0.25f);
            }
            else
            {
                SwapFacingMode();
                StartMovingToAttack(0.25f);
            }
        }
    }
    public float GetFaceModeMul()
    {
        return faceModeMul;
    }
    public void HeroRepositionToCenter()
    {
        transform.position = Vector3.zero;
        CamFollow.instance.CatchTargetInstant();
    }
    
    

}
