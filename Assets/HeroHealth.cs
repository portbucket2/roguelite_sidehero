﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HeroHealth : MonoBehaviour
{
    public float maxHealth;
    public float maxHealthApp
    {
        get
        {
            if(AbilityCardMultipliers.instance != null)
            {
                return (float)(int)((float)maxHealth * AbilityCardMultipliers.instance.maxHealthHeroMul);
            }
            else
            {
                return maxHealth ;
            }
            
        }
    }
    [SerializeField]
    float currentHeath;
    public Slider heroHelathSlider;
    public Text healthValueText;
    bool isDead;
    public bool superMoveMode;
    public ParticleSystem heroDamageParticle;
    public Action actionHeroDeath;
    
    HeroShieldSystem heroShieldSystem;
    // Start is called before the first frame update
    void Start()
    {
        //GetHeathSavedData();
        AreaSelectionUiManage.instance.actionAreaStartButton += InitialTasks;
        UiManage.instance.actionUiManageGameStartButton += InitialTasks;
        //InitialTasks();
        heroShieldSystem = GetComponent<HeroShieldSystem>();
        GameStatesControl.instance.actionLevelStart += UpdateHeathUi;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DamageHeathBy(float t)
    {
        if (isDead)
        {
            return;
        }
        if (superMoveMode)
        {
            return;
        }
        if (heroShieldSystem.ShieldEnabledOnNot())
        {
            heroShieldSystem.DamageShiledBy(t * AbilityCardMultipliers.instance.heroGetDamageMul);
            return;
        }
        currentHeath -= (t * AbilityCardMultipliers.instance.heroGetDamageMul) ;
        currentHeath = (int)Mathf.Clamp(currentHeath, 0, maxHealthApp);
        heroDamageParticle.Play();
        if(currentHeath <= 0)
        {
            KillHero();
        }
        else
        {
            ReferenceMaster.instance.heroAnimControl.GoGetHit();
        }
        UpdateHeathUi();

        ReferenceMaster.instance.superMoveUiManage.FillSuperBy(ReferenceMaster.instance.superMoveUiManage.fillIncreaseOnHitApp);
        SetHeathSavedData();
    }
    public void UpdateHeathUi()
    {
        heroHelathSlider.value = Mathf.Lerp(0, 100, currentHeath / maxHealthApp);
        healthValueText.text = "" + currentHeath + "/" + maxHealthApp;
        Debug.Log("maxH "+maxHealthApp);
    }
    public void InitialTasks()
    {
        
        HeroHealthRefill();
        UpdateHeathUi();
    }
    public void GetHeathSavedData()
    {
        currentHeath = PlayerPrefs.GetFloat("currentHeath", 100);
        UpdateHeathUi();
    }
    public void SetHeathSavedData()
    {
        PlayerPrefs.SetFloat("currentHeath", currentHeath );
    }
    public void KillHero()
    {
        //Destroy(gameObject);
        isDead = true;
        EnemySpawner.instance.StopAllEnemiesAttack();

        LevelManager.instance.ChangeLevelResultTypeTo(LevelResultType.Fail);
        GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult, 1);

        ReferenceMaster.instance.heroAnimControl.GoDead();
        actionHeroDeath?.Invoke();
    }
    public bool HeroDeadOrnot()
    {
        return isDead;
    }
    public void HeroHealthRefill()
    {
        currentHeath = maxHealthApp;
        SetHeathSavedData();
    }
    //public void HeroReset()
    //{
    //    HeroHealthRefill();
    //    UpdateHeathUi();
    //    isDead = false;
    //    ReferenceMaster.instance.heroAnimControl.GoAlive();
    //}
    public void HeroResetWithNoHeathRefill()
    {
        //HeroHealthRefill();
        if (isDead)
        {
            HeroHealthRefill();
        }
        UpdateHeathUi();
        isDead = false;
        ReferenceMaster.instance.heroAnimControl.GoAlive();
    }
    public void HeroHealthIncreaseBy(float per)
    {
        currentHeath += (int)((float)maxHealthApp * (per * 0.01f));
        currentHeath = Mathf.Clamp(currentHeath, 0, maxHealthApp);
        SetHeathSavedData();
        UpdateHeathUi();
    }
    public void HeroHealthRevive()
    {
        HeroResetWithNoHeathRefill();
    }
    
}
