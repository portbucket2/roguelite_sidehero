﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LionStudios.Runtime.Sdks;
using FRIA;
using UnityEngine.SceneManagement;

public class SplashScreenManager : MonoBehaviour
{
    ABManager.PlayerConfig playerConfig = new ABManager.PlayerConfig();

    // Start is called before the first frame update
    IEnumerator Start()
    {
        
        AppLovin.WhenInitialized(() =>
        {
            LionStudios.Runtime.Sdks.AppLovin.LoadRemoteData(playerConfig);
            ABManager.SetFetchedData(playerConfig);
            
        });
        while (!ABManager.isDataFetchComplete || TestABController.isAB_trialValueSetupPending)
        {
            yield return null;
        }
        SceneManager.LoadScene(1);
    }

    
}
