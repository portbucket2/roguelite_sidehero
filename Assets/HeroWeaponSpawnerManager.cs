﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroWeaponSpawnerManager : MonoBehaviour
{
    public HeroWeaponSpawner heroWeaponSpawner;
    HeroWeaponSystem heroWeaponSystem;
    //public List<HeroWeaponSpawnStations> heroWeaponSpawnStations;
    public int heroWeaponAppearCommonEnemySerialIndex;
    public static HeroWeaponSpawnerManager instance;
    [System.Serializable]
    public class HeroWeaponSpawnStations
    {
        public int levelIndex;
        public int enemyIndex;
        //public int weaponIndex;
        public HeroWeapons weapon;
    }
    public float lifetime;
    public float interval;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        EnemySpawner.instance.actionEnemySpawn += EvaluateWeaponSpawn;
        heroWeaponSystem = ReferenceMaster.instance.heroHealth.GetComponent<HeroWeaponSystem>();
        GameStatesControl.instance.actionLevelResult += LevelResultFun; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EvaluateWeaponSpawn()
    {
        int l = GameManagementMain.instance.levelIndex;
        int enemyInd = EnemySpawner.instance.GetEnemySpawnCount();

        /*
        for (int i = 0; i < heroWeaponSpawnStations.Count; i++)
        {
            if(heroWeaponSpawnStations[i].levelIndex == l)
            {
                if(heroWeaponSpawnStations[i].enemyIndex == enemyInd)
                {
                    if (!WeaponIsUnlockedOrNot(heroWeaponSpawnStations[i].weapon))
                    {
                        return;
                    }
                    float x = Random.Range(2f, 5f);
                    int s = Random.Range(0, 2);
                    if(s == 1)
                    {
                        x *= -1;
                    }
                    Vector3 spawnPos = ReferenceMaster.instance.heroHealth.transform.position + new Vector3(x,0,0);
                    GameObject go = Instantiate(heroWeaponSpawner.gameObject, spawnPos, Quaternion.identity);
                    go.GetComponent<HeroWeaponSpawner>().InitiateWeaponSpawner((int)heroWeaponSpawnStations[i].weapon);
                }
            }
        }
        */
        if (enemyInd == heroWeaponAppearCommonEnemySerialIndex)
        {
            if (heroWeaponSystem.unlockedWeapons.Count > 0)
            {
                
                SpawnWeaponSpawner();
            }
        }
        

        //Debug.Log("weaponSpawn " + l + " " + enemyInd);
    }
    public void SpawnWeaponSpawner()
    {
        if (heroWeaponSystem.heroWeaponCurrent != HeroWeapons.None)
        {
            return;
        }

        int w = heroWeaponSystem.unlockedWeapons[Random.Range(0, heroWeaponSystem.unlockedWeapons.Count)];

        float x = Random.Range(2f, 5f);
        int s = Random.Range(0, 2);
        if (s == 1)
        {
            x *= -1;
        }
        Vector3 spawnPos = ReferenceMaster.instance.heroHealth.transform.position + new Vector3(x, 0, 0);
        GameObject go = Instantiate(heroWeaponSpawner.gameObject, spawnPos, Quaternion.identity);
        go.GetComponent<HeroWeaponSpawner>().InitiateWeaponSpawner(w + 1);
    }
    public void SpawnWeaponSpawnerDelayed(float d)
    {
        
        StopAllCoroutines();
        StartCoroutine(WeaponSpawnCoroutine(d));
    }
    IEnumerator WeaponSpawnCoroutine(float d)
    {
        yield return new WaitForSeconds(d);
        SpawnWeaponSpawner();
    }
    public bool WeaponIsUnlockedOrNot(HeroWeapons weapon)
    {
        bool r = false;
        for (int i = 0; i < heroWeaponSystem.unlockedWeapons.Count; i++)
        {
            if(heroWeaponSystem.unlockedWeapons[i] == ((int)weapon - 1))
            {
                r = true;
                break;
            }
        }

        return r;
    }
    public void LevelResultFun()
    {
        StopAllCoroutines();
        
    }
}
