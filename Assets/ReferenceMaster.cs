﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceMaster : MonoBehaviour
{
    public static ReferenceMaster instance;
    public HeroHealth heroHealth;
    public HeroAnimControl heroAnimControl;
    public SuperMoveUiManage superMoveUiManage;
    //public SuperMoveUnlockManager superMoveUnlockManager;

    private void Awake()
    {
        instance = this;
        heroHealth = FindObjectOfType<HeroHealth>();
        heroAnimControl = FindObjectOfType<HeroAnimControl>();


        //superMoveUiManage = FindObjectOfType<SuperMoveUiManage>();
        //superMoveUnlockManager = FindObjectOfType<SuperMoveUnlockManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
