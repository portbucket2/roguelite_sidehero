﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroWeaponUnlockedUi : MonoBehaviour
{
    public List<int> levelsOFHeroWeaponsUiPanel;
    //public GameObject buttonGoToHeroWeapon;
    //public GameObject buttonGoToLevelEnd;
    public GameObject panelHeroWeaponUnlock;
    HeroWeaponSystem heroWeaponSystem;
    public int currentWeaponIndexToBeUnlocked;
    public Text unlockedWeaponTitleText;
    public Image weaponIconImage;
    // Start is called before the first frame update
    void Start()
    {
        heroWeaponSystem = ReferenceMaster.instance.heroHealth.GetComponent<HeroWeaponSystem>();
        GameStatesControl.instance.actionLevelResult += EvaluateButtonsAndWeaponUnlockIndexUpdate;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EvaluateButtonsAndWeaponUnlockIndexUpdate()
    {
        
        //buttonGoToHeroWeapon.SetActive(false);
        //buttonGoToLevelEnd.SetActive(true);
        panelHeroWeaponUnlock.SetActive(false);
        for (int i = 0; i < levelsOFHeroWeaponsUiPanel.Count; i++)
        {
            if (GameManagementMain.instance.levelIndexDisplayed == levelsOFHeroWeaponsUiPanel[i])
            {
                currentWeaponIndexToBeUnlocked = heroWeaponSystem.GetLockedWeaponIndex();
                if (currentWeaponIndexToBeUnlocked >= 0)
                {
                    //buttonGoToHeroWeapon.SetActive(true);
                    //buttonGoToLevelEnd.SetActive(false);
                    //unlockedWeaponTitleText.text = "" + (HeroWeapons)(currentWeaponIndexToBeUnlocked + 1);
                    unlockedWeaponTitleText.text = "" + ReferenceMaster.instance.heroHealth.GetComponent<HeroWeaponSystem>().weapons[currentWeaponIndexToBeUnlocked].GetComponent<HeroWeapon>().title;
                    weaponIconImage.sprite = ReferenceMaster.instance.heroHealth.GetComponent<HeroWeaponSystem>().weapons[currentWeaponIndexToBeUnlocked].GetComponent<HeroWeapon>().weaponIcon;
                    return;
                }
               
            }
        }


        //if (heroWeaponSystem.GetLockedWeaponIndex() >= 0)
        //{
        //    buttonGoToHeroWeapon.SetActive(true);
        //    buttonGoToLevelEnd  .SetActive(false);
        //}
        //else
        //{
        //    buttonGoToHeroWeapon.SetActive(false);
        //    buttonGoToLevelEnd.SetActive(true);
        //}
    }
    public void EvaluateGotItButtonFun()
    {
        bool b = false;
        
        panelHeroWeaponUnlock.SetActive(false);
        for (int i = 0; i < levelsOFHeroWeaponsUiPanel.Count; i++)
        {
            if (GameManagementMain.instance.levelIndexDisplayed == levelsOFHeroWeaponsUiPanel[i])
            {
                currentWeaponIndexToBeUnlocked = heroWeaponSystem.GetLockedWeaponIndex();
                if (currentWeaponIndexToBeUnlocked >= 0)
                {
                    b = true;
                    //unlockedWeaponTitleText.text = "" + (HeroWeapons)(currentWeaponIndexToBeUnlocked + 1);
                    break;
                }

            }
        }

        if (b)
        {
            ButtonOkFun();
        }
        else
        {
            UiManage.instance.GotItButtonFun();
        }
    }
    public void ButtonOkFun()
    {
        panelHeroWeaponUnlock.SetActive(true);
    }

    public void CloseHeroWeaponUi()
    {
        panelHeroWeaponUnlock.SetActive(false);
    }
    public void UnlockHeroWeapon()
    {
        heroWeaponSystem.UnlockWeapon(currentWeaponIndexToBeUnlocked);
    }
}
