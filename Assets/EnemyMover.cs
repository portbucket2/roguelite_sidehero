﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    
    public float moveSpeed;
    public LayerMask heroLayer;
    public LayerMask enemyLayer;
    public float enemyMoveStopRange;
    bool moving;
    float moveSpeedMul;
    EnemyAttackSystem enemyAttackSystem;
    EnemyHealth enemyHealth;
    public GameObject raycastedObj;
    public bool gameReviving;
    // Start is called before the first frame update
    void Start()
    {
        enemyAttackSystem = GetComponent<EnemyAttackSystem>();
        enemyHealth = GetComponent<EnemyHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHealth.isDead) { return; }
        MoveEnemy();
    }

    public void MoveEnemy()
    {
        if (enemyAttackSystem.waitForExplosion)
        {
            return;
        }
        raycastedObj = null;
        raycastedObj = RayCastForPlayerOrEnemy();
        if (raycastedObj == null)
        {
            if (enemyAttackSystem.attackAnimPlaying)
            {
                //return;
            }
            else
            {
                if (!moving)
                {
                    moving = true;
                    enemyAttackSystem.PauseAttackLoop();
                    GetComponent<EnemyAnimControl>().GorRun();

                    //transform.Translate(0, 0, moveSpeed * Time.deltaTime);
                }
                moveSpeedMul = Mathf.MoveTowards(moveSpeedMul, 1, Time.deltaTime * 10);
            }
            
        }
        else
        {
            if (enemyAttackSystem.attackAnimPlaying)
            {
                //return;
            }
            else
            {
                
            }
            if (moving)
            {
                moving = false;
                GetComponent<EnemyAnimControl>().GoRunStop();
                if (raycastedObj.GetComponent<HeroMover>() != null)
                {
                    enemyAttackSystem.EnableAttackLoop();

                }
                switch (enemyAttackSystem. enemyType)
                {
                    case EnemyType.Ordinary:
                        {

                            break;
                        }
                    case EnemyType.Hammer:
                        {

                            break;
                        }
                    case EnemyType.Molotov:
                        {
                            //enemyAttackSystem.EnableAttackLoop();
                            break;
                        }
                    case EnemyType.Grenade:
                        {
                            enemyAttackSystem.EnableAttackLoop();
                            break;
                        }
                }


            }
            else
            {
                if (gameReviving)
                {
                    if (raycastedObj.GetComponent<HeroMover>() != null)
                    {
                        if (!raycastedObj.GetComponent<HeroHealth>().HeroDeadOrnot())
                        {
                            enemyAttackSystem.EnableAttackLoop();
                        }


                    }
                    gameReviving = false;
                }
                
            }

            moveSpeedMul = Mathf.MoveTowards(moveSpeedMul, 0, Time.deltaTime * 10);
            //transform.Translate(0, 0, moveSpeed * Time.deltaTime * 20f);
        }
        transform.Translate(0, 0, moveSpeed * moveSpeedMul* Time.deltaTime);
    }

    public GameObject RayCastForPlayerOrEnemy()
    {
        RaycastHit hit;
        
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position + new Vector3(0, 0.5f, 0), transform.forward , out hit, enemyMoveStopRange , heroLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            return hit.transform.gameObject;
        }
        else
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
            return null;
        }
    }
    public void GameRevivingStartForEnemy()
    {
        gameReviving = true;
    }
    
}
