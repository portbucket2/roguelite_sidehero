﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndCardsUi : MonoBehaviour
{
    public GameObject parentUiObj;
    public static LevelEndCardsUi instance;
    public CardButton[] cardButtons;
    public GameObject[] cardPosRefs;
    public int clickedButtonIndex;
    public GameObject ButtonNext;
    public GameObject ButtonToAreaClear;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionMenuIdle += DisAppearCardsUi;
        GameStatesControl.instance.actionGameStart += DisAppearCardsUi;

        AssignAbilitiesToTheCards();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisAppearCardsUi()
    {
        parentUiObj.SetActive(false);
    }
    public void AppearCardsUi()
    {
        parentUiObj.SetActive(true);

        CardsDragToCenter();
        DisAppearNextButtons();
        AssignAbilitiesToTheCards();

        for (int i = 0; i < cardButtons.Length; i++)
        {
            cardButtons[i].ChangeCardClickStateTo(CardButton.CardClickState.Clickable);
        }

    }
    public void CardsDragToCenter()
    {
        for (int i = 0; i < cardButtons.Length; i++)
        {
            cardButtons[i].gameObject.transform.position = cardPosRefs[i].transform.position;
            cardButtons[i].gameObject.transform.localScale = Vector3.one;
            cardButtons[i].GetComponent<UiTransitionCustom>().TransitionUpOrDownToCenter(Screen.height, 0, 0, 2);
        }
    }
    public void CardsGoAwayAfterSelection()
    {
        for (int i = 0; i < cardButtons.Length; i++)
        {
            if(i == clickedButtonIndex)
            {
                cardButtons[i].GetComponent<UiTransitionCustom>().TransitionScaleTo(1, 1.2f, 0, 2);
                cardButtons[i].ChangeCardClickStateTo(CardButton.CardClickState.Clicked);
                //cardButtons[i].GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(cardButtons[i].gameObject.transform.localPosition.x, 0, 0, 2);
            }
            else
            {
                //cardButtons[i].GetComponent<UiTransitionCustom>().TransitionUpOrDownToCenter(0, -Screen.height, 0, 2);
                cardButtons[i].ChangeCardClickStateTo(CardButton.CardClickState.LockedForAd);
            }
            
        }
    }
    public void AppearNextButtons()
    {
        //if (!AreaSelectionUiManage.instance.EvaluateLastLevelOfThisAreaOrNot())
        //{
        //    ButtonNext.SetActive(true);
        //    ButtonToAreaClear.SetActive(false);
        //}
        //else
        //{
        //    ButtonToAreaClear.SetActive(true);
        //    ButtonNext.SetActive(false);
        //}
        ButtonNext.SetActive(true);
        ButtonToAreaClear.SetActive(false);
    }
    public void DisAppearNextButtons()
    {
        ButtonNext.SetActive(false);
        ButtonToAreaClear.SetActive(false);
    }

    public void AssignAbilitiesToTheCards()
    {
        List<int> healthAbiliesIndex = new List<int>();
        List<int> OrdinaryAbiliesIndex = new List<int>();
        List<int> selectedIndexes = new List<int>();
        for (int i = 0; i < AbilitiesManager.instance.abilities.Count; i++)
        {
            if (AbilitiesManager.instance.abilities[i].heathType)
            {
                healthAbiliesIndex.Add(AbilitiesManager.instance.abilities[i].abilityIndex);
            }
            else
            {
                OrdinaryAbiliesIndex.Add(AbilitiesManager.instance.abilities[i].abilityIndex);
            }
        }
        if (OrdinaryAbiliesIndex.Count <= 0)
        {
            return;
        }

        int a = Random.Range(0, OrdinaryAbiliesIndex.Count );
        selectedIndexes.Add(OrdinaryAbiliesIndex[a]);
        OrdinaryAbiliesIndex.RemoveAt(a);

        a = Random.Range(0, OrdinaryAbiliesIndex.Count );
        selectedIndexes.Add(OrdinaryAbiliesIndex[a]);
        //OrdinaryAbiliesIndex.RemoveAt(a);

        a = Random.Range(0, healthAbiliesIndex.Count );
        selectedIndexes.Add(healthAbiliesIndex[a]);

        for (int i = 0; i < selectedIndexes.Count; i++)
        {
            cardButtons[i].abilityIndexChosen = selectedIndexes[i];
            cardButtons[i].CardInfoUpdate(cardButtons[i].abilityIndexChosen);
        }
        //OrdinaryAbiliesIndex.RemoveAt(a);
        //Debug.Log(selectedIndexes[0]+""+ selectedIndexes[1]+""+ selectedIndexes[2]);
        Debug.Log(healthAbiliesIndex[0] + "" + healthAbiliesIndex[1] );

    }
}
