Enemy Animations
==================

Machete Attack		: 0-68
Machete Idle		: 75-115

Dagger Attack		: 120-200
Dagger Idle		: 205-235

Baseball Bat Attack	: 240-300
Baseball Bat Idle	: 310-340
				
Crowbar Attack		: 350-410
Crowbar Idle		: 420-450

Pickaxe Attack		: 460-510
Pickaxe Idle		: 515-545

Throwing Knife Attack	: 550-640
Throwing Knife Idle	: 650-680

Shuriken Attack		: 690-735
Shuriken Idle		: 740-770


Instructions
---------------------------------
0-20 frames anticipation
20-30 frames hit animation
30 frame hit