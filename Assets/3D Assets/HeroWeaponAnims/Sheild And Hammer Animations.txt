Hero Shield Anim
=================

Idle Animation			: 0-62

Hit 1	- Forward punch		: 70-95
Hit 2	- Left to right		: 100-125
Hit 3	- Upper cut		: 130-155
Hit 4	- 360 spin attack	: 160-185

Throw				: 190-215



Hero Hammer Anim
=================

Hit 1	- Right to Left		: 0-25
Hit 2	- Left to right		: 30-55
Hit 3	- 360 spin attack	: 60-85
Hit 4	- Upper cut		: 90-115

Throw				: 120-145