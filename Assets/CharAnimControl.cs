﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnimControl : MonoBehaviour
{
    public Animator charAnim;
    public Vector2 animBlendKnobPos;
    public Vector2 animBlendKnobTarPos;
    public float snapSpeed;
    protected HeroMover heroMover;
    int attackAnimIndex;
    public float faceModeMul;
    // Start is called before the first frame update
    void Start()
    {
        
        //GoIdle();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoIdle()
    {
        
        animBlendKnobTarPos.x = 1.0f * faceModeMul;
        
    }
    public void GoPunchOne()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("punchOne");
    }
    public void GoPunchTwo()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("punchTwo");
    }
    public void GoKickOne()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("kickOne");
    }
    public void GoKickTwo()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("kickTwo");
    }
    public void GoGetHit()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("hit");
    }
    public void GorRun()
    {
        charAnim.SetBool("run", true);
    }
    public void GoRunStop()
    {
        charAnim.SetBool("run", false);
    }
    public void GoDodge()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("dodge");
    }
    public void GoDead()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetBool("dead", true);
    }
    public void GoAlive()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetBool("dead", false);
    }
    public void GoPunchEnemy()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("punchEnemy");
    }
    public void GoHammerHit()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("hammerHit");
    }
    public void GoGranadeThrow()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("granadeThrow");
    }
    public void GoMolotovThrow()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("molotovThrow");
    }
    public void GoSuperMove(string s = "superEarth")
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger(s);
    }
    public void GoSurikenThrow()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("suriken");
    }
    public void GoMachetteAttack()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("machette");
    }
    public void GoDaggerAttack()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("dagger");
    }
    public void GoBaseballAttack()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("baseball");
    }
    public void GoCrowbarAttack()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("crowbar");
    }
    public void GoPickaxeAttack()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("pickaxe");
    }
    public void GoKnifeThrow()
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("knife");
    }
    public void GoHeroWeaponThrow()
    {
        animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetTrigger("heroWeaponThrow");
    }
    public void HeroWeaponIndexSetTo(float f)
    {
        //animBlendKnobTarPos.x = 1f * faceModeMul;
        charAnim.SetFloat("HeroWeaponIndex",f);
    }
    public void AttackAnimPlay()
    {
        int index = attackAnimIndex % 4;
        attackAnimIndex += 1;
        ParticlesFxManager.instance.EnableWeaponTrail();
        switch (index)
        {
            case 0:
                {
                    ParticlesFxManager.instance.EnableTrail(1,faceModeMul);
                    GoPunchOne();
                    break;

                }
            case 1:
                {
                    ParticlesFxManager.instance.EnableTrail(0,faceModeMul);
                    GoPunchTwo();
                    break;

                }
            case 2:
                {
                    ParticlesFxManager.instance.EnableTrail(2, faceModeMul);
                    GoKickOne();
                    break;

                }
            case 3:
                {
                    ParticlesFxManager.instance.EnableTrail(2, faceModeMul);
                    GoKickTwo();
                    break;

                }
            default:
                {
                    GoPunchOne();
                    break;
                }
        }
    }
}
