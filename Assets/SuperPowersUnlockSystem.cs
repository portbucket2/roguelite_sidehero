﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuperPowersUnlockSystem : MonoBehaviour
{

    public static SuperPowersUnlockSystem instance;
    public List<SuperMove> superMovesAvailable;
    public float fillPercentage;
    public SuperMove superMoveCurrent;
    SuperMove superOnProgress;
    public GameObject screenBlock;

    [Header("UiVisual")]
    public Image iconImageUi;
    public Image iconFillImageUi;
    public Text percentageTextUi;
    public Text superTitleTextUi;
    int t;
    public bool superMoveUnlocked;
    public int superMoveAllUnlockedSavedData;
    int superMoveAllUnlockedSavedDataReviveValue;
    public SuperMoveUiManage superMoveUiManage;
    public int allSuperUnlockedLevelIntervalCountdown;
    public Text buttonText;
    [System.Serializable]
    public class SuperMove{
        public string title;
        public string trigger;
        //public int unlockFrom;
        public int unlockAtLevelIndex;
        public Sprite superIconImageSprite;
        public int superIndex;
        public bool unlocked;

    }

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //LevelEndSuperUnlockCall(3);
        GameStatesControl.instance.actionGameStart += SavedDataEvaluateForSuperMoveUnlock;
        GameStatesControl.instance.actionLevelStart += EvaluateSuperMoveUnlockedorNot;

        //ReferenceMaster.instance.heroHealth.actionHeroDeath += ReviveSaveAllSuperUnlockSaveData;
        //LevelFailedCountdownSystem.instance.actionReviveHero += ReviveDataGetBackSuperUnlockSaveData;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            //LevelEndSuperUnlockCall(t);
            //t += 1;
        }
    }

    IEnumerator SetFillPercentage(float from, float to, float val = 0, float speed = 1f)
    {
        yield return new WaitForSeconds(0);
        val += Time.deltaTime * speed;
        val = Mathf.Clamp(val, 0, 1);
        fillPercentage = Mathf.Lerp(from, to, val);

        percentageTextUi.text = "" + (int)fillPercentage + " % UNLOCKED";
        iconFillImageUi.fillAmount = fillPercentage* 0.01f;
        if(val == 1)
        {
            UnlockSuoerUiTouch();
        }
        if (val < 1)
        {
            StartCoroutine(SetFillPercentage(from, to, val, speed));
        }
    }

    public void LevelEndSuperUnlockCall(int levelIndex)
    {
        //Hard Coded Correction For special case
        //SpecialCorrectionHardCodedAtLevelEnd(levelIndex);
        //HardCode

        if (CheckAllSuperMoveUnlockedorNot())
        {
            int r = Random.Range(0, (superMovesAvailable.Count - 1));
            superOnProgress = superMovesAvailable[r];
            superMoveCurrent = superOnProgress;
            percentageTextUi.text =  " NEW SUPER";
            UpdateLevelEndSuperUi(superOnProgress);
            return;
        }

        BlockSuoerUiTouch();
        //int levelIndex = GameManagementMain.instance.levelIndexDisplayed;
        int minIndex = -1;
        int maxIndex = 1000;
        
        for (int i = 0; i < superMovesAvailable.Count; i++)
        {
            if( superMovesAvailable[i].unlockAtLevelIndex >= levelIndex)
            {
                if (superMovesAvailable[i].unlockAtLevelIndex <= maxIndex)
                {
                    maxIndex = superMovesAvailable[i].unlockAtLevelIndex;
                    superOnProgress = superMovesAvailable[i];
                }
                    
            }

            if(superMovesAvailable[i].unlockAtLevelIndex <= levelIndex)
            {
                superMovesAvailable[i].unlocked = true;
                if (superMovesAvailable[i].unlocked)
                {
                    superMoveCurrent = superMovesAvailable[i];
                }
                
                
            }
            
        }
        for (int i = 0; i < superMovesAvailable.Count; i++)
        {
            if (superMovesAvailable[i].unlockAtLevelIndex < levelIndex)
            {
                if (superMovesAvailable[i].unlockAtLevelIndex >= minIndex)
                {
                    minIndex = superMovesAvailable[i].unlockAtLevelIndex;
                }

            }

        }
        if(maxIndex == minIndex)
        {
            return;
        }
        float percentageTo =100f* ((float)levelIndex - (float)minIndex) /( (float)maxIndex - (float)minIndex);
        float percentageFrom = 100f * ((float)(levelIndex-1) - (float)minIndex) / ((float)maxIndex - (float)minIndex);
        if(levelIndex == minIndex)
        {
            percentageFrom = 0;
        }
        //Debug.Log(levelIndex+" , "+ minIndex + " , " + maxIndex+" , "+ percentageFrom+" , "+percentageTo);
        StartCoroutine(SetFillPercentage(percentageFrom, percentageTo, 0, 1));
        if(percentageTo >= 100)
        {
            UpdateButtonText("GOT IT!");
        }
        else
        {
            UpdateButtonText("OK");
        }
        UpdateLevelEndSuperUi(superOnProgress);

        
    }
    public void UpdateLevelEndSuperUi(SuperMove s)
    {
        superTitleTextUi.text = s.title;
        //iconImageUi.sprite = s.superIconImageSprite;
        //iconFillImageUi.sprite = s.superIconImageSprite;
    }
    public void BlockSuoerUiTouch()
    {
        screenBlock.SetActive(true);
    }
    public void UnlockSuoerUiTouch()
    {
        screenBlock.SetActive(false);
        
    }
    public void EvaluateSuperMoveUnlockedorNot()
    {
        superMoveUnlocked = false;
        int index = GameManagementMain.instance.levelIndexDisplayed;
        int highestSuperLevelIndex = -1;
        for (int i = 0; i < superMovesAvailable.Count; i++)
        {
            if (superMovesAvailable[i].unlockAtLevelIndex < index)
            {

                superMoveUnlocked = true;
                if(superMovesAvailable[i].unlockAtLevelIndex > highestSuperLevelIndex)
                {
                    highestSuperLevelIndex = superMovesAvailable[i].unlockAtLevelIndex;
                    superMovesAvailable[i].unlocked = true;
                    superMoveCurrent = superMovesAvailable[i];
                }
            }
        }

        if (CheckAllSuperMoveUnlockedorNot())
        {
            if(superOnProgress != null)
            {
                superMoveCurrent = superOnProgress;
            }
            else
            {
                superMoveCurrent = superMovesAvailable[0];
            }
        }
    }
    public bool CheckAllSuperMoveUnlockedorNot()
    {
        bool allSuperUnlocked = true;
        for (int i = 0; i < superMovesAvailable.Count; i++)
        {
            if (!superMovesAvailable[i].unlocked )
            {
                allSuperUnlocked = false;
            }
        }
        if (allSuperUnlocked)
        {
            superMoveAllUnlockedSavedData = 1;
            //PlayerPrefs.SetInt("superMoveAllUnlockedSavedData", superMoveAllUnlockedSavedData);
            superMoveUnlocked = true;
        }
        return allSuperUnlocked;
    }

    public void SpecialCorrectionHardCodedAtLevelEnd(int levelIndex)
    {
        if(superMovesAvailable[0].unlockAtLevelIndex == levelIndex && superMoveUiManage!=null)
        {
            superMoveUiManage.SpecialSuperFillFull();
            Debug.Log("working");
        }
    }
    public void SavedDataEvaluateForSuperMoveUnlock()
    {
        //superMoveAllUnlockedSavedData = PlayerPrefs.GetInt("superMoveAllUnlockedSavedData", 0);
        if (superMoveAllUnlockedSavedData == 1)
        {
            for (int i = 0; i < superMovesAvailable.Count; i++)
            {
                superMovesAvailable[i].unlocked = true;
            }
        }
        else
        {
            for (int i = 0; i < superMovesAvailable.Count; i++)
            {
                superMovesAvailable[i].unlocked = false;
            }
        }
        if(GameManagementMain.instance.levelIndex == 0)
        {
            for (int i = 0; i < superMovesAvailable.Count; i++)
            {
                superMovesAvailable[i].unlocked = false;
            }
            Debug.Log("Level Zero");
        }
    }
    public void SuperMoveUiAppearOrNot()
    {
        bool appear = true;
        if(superMoveAllUnlockedSavedData >= 1)
        {
            appear = false;
            allSuperUnlockedLevelIntervalCountdown += 1;
            if(allSuperUnlockedLevelIntervalCountdown >= 5)
            {
                appear = true;
                allSuperUnlockedLevelIntervalCountdown = 0;
            }
        }
        //appear = true;
        if (appear)
        {
            SuperPowersUnlockSystem.instance.LevelEndSuperUnlockCall(GameManagementMain.instance.levelIndexDisplayed);
        }
        else
        {
            UiManage.instance.GetComponent<HeroWeaponUnlockedUi>().EvaluateGotItButtonFun();
        }
    }

    private void OnEnable()
    {
        //UpdateButtonText( "OK");
    }
    public void UpdateButtonText(string s)
    {
        buttonText.text = s;
    }
    //public void ReviveSaveAllSuperUnlockSaveData()
    //{
    //    superMoveAllUnlockedSavedDataReviveValue = superMoveAllUnlockedSavedData;
    //    superMoveAllUnlockedSavedData = 0;
    //    superMoveUnlocked = false;
    //    //PlayerPrefs.SetInt("superMoveAllUnlockedSavedData", superMoveAllUnlockedSavedData);
    //}
    //public void ReviveDataGetBackSuperUnlockSaveData()
    //{
    //    superMoveAllUnlockedSavedData = superMoveAllUnlockedSavedDataReviveValue;
    //    if (superMoveAllUnlockedSavedData == 1)
    //    {
    //        superMoveUnlocked = true;
    //    }
    //    //PlayerPrefs.SetInt("superMoveAllUnlockedSavedData", superMoveAllUnlockedSavedData);
    //}
}
