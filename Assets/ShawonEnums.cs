﻿public enum FacingMode
{
    Right,Left
}
public enum GameStates
{
    MenuIdle,
    GameStart,
    LevelStart,
    GamePlay,
    LevelResult,
    LevelEnd
}
public enum LevelResultType
{
    Success, Fail
}
public enum EnemyType
{
    Ordinary, Hammer, Grenade, Molotov , Suriken,Machette,Dagger,Baseball,Crowbar,PickAxe,Knife
}
public enum EnemyCategory
{
    CloseRange, MidRange, LongOverhead, LongHorizontal
}
public enum HeroAttackState
{
    HeroIdle, HeroAttacking
}
public enum HeroWeapons
{
    None,Shield,Hammer
}
public enum InputTouchMode
{
    Swipe, Tap
}
