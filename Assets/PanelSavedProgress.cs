﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PanelSavedProgress : MonoBehaviour
{
    public GameObject panelObj;
    public static PanelSavedProgress instance;
    public Action actionGameResume;
    public Action actionNoGameResume;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AppearSavedProgressPanel()
    {
        panelObj.SetActive(true);
    }
    public void DisAppearSavedProgressPanel()
    {
        panelObj.SetActive(false);
    }
    public void DecideAppearOrNotSavedProgressPanel()
    {
        if(GameManagementMain.instance.levelIndex == 0)
        {
            DisAppearSavedProgressPanel();
        }
        else
        {
            if(LevelManager.instance.levelResultType == LevelResultType.Fail)
            {
                DisAppearSavedProgressPanel();
            }
            else
            {
                AppearSavedProgressPanel();
            }
            
        }
    }
    public void ButtonYesFun()
    {
        actionGameResume?.Invoke();
        UiManage.instance.StartButtonFun();
        DisAppearSavedProgressPanel();
        
    }
    public void ButtonNoFun()
    {
        actionNoGameResume?.Invoke();
        int levelIndex = AreaSelectionUiManage.instance.areaUiPrefabs[AreaSelectionUiManage.instance.currentAreaIndex].GetComponent<AreaUI>().minLevelIndex;
        
        GameManagementMain.instance.LevelUpdateTo(levelIndex);
        DisAppearSavedProgressPanel();
        UiManage.instance.UiValuesUpdate();
    }
}
