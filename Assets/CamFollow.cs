﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    
    public GameObject target;
    GameObject Hero;
    public float sensitivity;
    public static CamFollow instance;
    bool moveTowardsMode;
    //public AnimationCurve animFov ;
    private void Awake()
    {
        instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        Hero = ReferenceMaster.instance.heroHealth.gameObject;
        TargetChangeToHero();
    }

    // Update is called once per frame
    void Update()
    {
       if(target != null)
        {
            if (moveTowardsMode)
            {
                
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * sensitivity*2);
                //transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * sensitivity);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, target.transform.position, Time.deltaTime * sensitivity);
            }
            //
            
            //Vector3 r = Quaternion.ToEulerAngles(target.transform.rotation) * Mathf.Rad2Deg;
            //Quaternion q = Quaternion.Euler(new Vector3(r.x*0.5f, 0, 0));
            //transform.rotation = Quaternion.Lerp(transform.rotation, q, Time.deltaTime * 5);

        } 
       
    }
    public void TargetChangeTo(GameObject g)
    {
        target = g;
        moveTowardsMode = true;
        Invoke("TargetChangeToHero", 2);
    }
    public void TargetChangeToHero()
    {
        target = Hero;
        moveTowardsMode = false;
    }
    public void CatchTargetInstant()
    {
        transform.position = target.transform.position;
    }


}
