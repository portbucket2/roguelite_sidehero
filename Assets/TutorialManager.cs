﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    public GameObject LeftRightText;
    public bool tutDone;
    public Text tutText;
    
    // Start is called before the first frame update
    void Start()
    {
        LeftRightText.SetActive(false);
        GameStatesControl.instance.actionLevelStart += EnableTutorial;
        InputMouseSystem.instance.actionSwipeRight += DisableTutorial;
        InputMouseSystem.instance.actionSwipeLeft += DisableTutorial;
        InputTouchModeControl.instance.ActionRightLeftButClick += DisableTutorial;

        GameStatesControl.instance.actionLevelResult += DisableTutorialInstant;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableTutorial()
    {
        if (tutDone)
        {
            return;
        }
        TextUpdateAccordingToInputTouchMode();
        LeftRightText.SetActive(false);
        if (GameManagementMain.instance.levelIndexDisplayed == 0)
        {
            LeftRightText.SetActive(true);
        }
        else
        {
            LeftRightText.SetActive(false);
        }
        GameStatesControl.instance.actionGamePlay -= EnableTutorial;
        //Debug.Log("tutEn");
    }
    public void DisableTutorial()
    {
        StartCoroutine(disableTutCoroutine(2));
        InputMouseSystem.instance.actionSwipeRight -= DisableTutorial;
        InputMouseSystem.instance.actionSwipeLeft -= DisableTutorial;
        InputTouchModeControl.instance.ActionRightLeftButClick -= DisableTutorial;

        //Debug.Log("tutDis");
    }
    public void DisableTutorialInstant()
    {
        StartCoroutine(disableTutCoroutine(0));
        InputMouseSystem.instance.actionSwipeRight -= DisableTutorial;
        InputMouseSystem.instance.actionSwipeLeft -= DisableTutorial;
        InputTouchModeControl.instance.ActionRightLeftButClick -= DisableTutorial;

        //Debug.Log("tutDis");
    }

    IEnumerator disableTutCoroutine(float t )
    {
        yield return new WaitForSeconds(t);
        LeftRightText.SetActive(false);
        tutDone = true;

    }
    public void TextUpdateAccordingToInputTouchMode()
    {
        InputTouchMode inputTouchModeCurrent = InputTouchModeControl.instance. inputTouchModeCurrent;
        switch (inputTouchModeCurrent)
        {
            case InputTouchMode.Swipe:
                {
                    tutText.text = "Swipe LEFT/RIGHT to Fight";
                    break;
                }
            case InputTouchMode.Tap:
                {
                    tutText.text = "Tap LEFT/RIGHT to Fight";
                    break;
                }
        }
    }
}
