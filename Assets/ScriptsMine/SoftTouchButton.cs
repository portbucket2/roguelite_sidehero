﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class SoftTouchButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    private bool touched;
    private int pointerID;
    
    public GameObject player;
    public Action ActionButtonFun;
    public Animator buttonAnim;
    void Awake()
    {
        touched = false;
    }

    //void Update()
    //{
    //    if (Input.GetTouch(0)) { player.GetComponent<PlayerController>().GoLeft(); }
    //}

    public void OnPointerDown(PointerEventData data)
    {
        //if (!touched)
        //{
        //    touched = true;
        //    pointerID = data.pointerId;
        //    canFire = true;
        //    //player.GetComponent<PlayerController>().Right = true;
        //}
        buttonAnim.SetTrigger("Pressed");
        ActionButtonFun?.Invoke();
        //Debug.Log("RightTap");
    }

    public void OnPointerUp(PointerEventData data)
    {
        //if (data.pointerId == pointerID)
        //{
        //    canFire = false;
        //    touched = false;
        //    //player.GetComponent<PlayerController>().Right = false;
        //}
    }

    

}

