﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelValuesCurrent : MonoBehaviour
{
    public static LevelValuesCurrent instance;
    public List<GameObject> enemyListCurrentLevel;
    public List<GameObject> enemyAliveCurrentLevel;
    public List<float> enemyAppearIntervals;
    public List<FacingMode> enemySpawnDirection;
    public List<bool> pauseSpawn;
    public bool hasBossFight;
    public List<GameObject> bossListCurrentLevel;
    public List<GameObject> bossAliveCurrentLevel;
    public int totalEnemiesCountInCurrentLevel;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitialTasks(int levelIndex)
    {
        enemyListCurrentLevel.Clear();
        bossListCurrentLevel.Clear();

        totalEnemiesCountInCurrentLevel = GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].enemyListInLevel.Count ;
        if (hasBossFight)
        {
            totalEnemiesCountInCurrentLevel = GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].enemyListInLevel.Count+ GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].bossListInLevel.Count;
        }
            //int levelIndex = GameManagementMain.instance.levelIndex;
            List<GameObject> list = new List<GameObject>();
        list = GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].enemyListInLevel;
        for (int i = 0; i < list.Count; i++)
        {
            enemyListCurrentLevel.Add(list[i]);
        }
        EnemySpawner.instance.enemyCountThisLevel = enemyListCurrentLevel.Count;

        hasBossFight = GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].hasBossFight;
        if (hasBossFight)
        {
            List<GameObject> bossList = new List<GameObject>();
            bossList = GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].bossListInLevel;
            for (int i = 0; i < bossList.Count; i++)
            {
                bossListCurrentLevel.Add(bossList[i]);
            }
            EnemySpawner.instance.bossCountThisLevel = bossListCurrentLevel.Count;
        }
        

        List<float> intervals = new List<float>(GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].enemyAppearIntervals);
        enemyAppearIntervals.Clear();
        enemyAppearIntervals = new List<float>(intervals);

        List<FacingMode> f = new List<FacingMode>(GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].enemySpawnDirection);
        enemySpawnDirection.Clear();
        enemySpawnDirection = new List<FacingMode>(f);

        //List<bool> pauseSpawn;
        List<bool> p = new List<bool> (GetComponent<LevelVauesAssignManager>().levelValues[levelIndex].pauseSpawn);
        pauseSpawn.Clear();
        pauseSpawn = new List<bool>(p);

        EnemySpawner.instance.StartEnemySpawning();
    }
    public void ResetIt()
    {
        while(enemyAliveCurrentLevel.Count > 0)
        {
            GameObject go = enemyAliveCurrentLevel[0];
            enemyAliveCurrentLevel.RemoveAt(0);
            Destroy(go);
        }
        while (bossAliveCurrentLevel.Count > 0)
        {
            GameObject go = bossAliveCurrentLevel[0];
            bossAliveCurrentLevel.RemoveAt(0);
            Destroy(go);
        }
        enemyListCurrentLevel.Clear();
        bossListCurrentLevel.Clear();
    }

}
