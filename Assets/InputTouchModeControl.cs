﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using LionStudios.Runtime.Sdks;
using FRIA;



public class InputTouchModeControl : MonoBehaviour
{
    //ABManager.PlayerConfig playerConfig = new ABManager.PlayerConfig();
    public InputTouchMode inputTouchModeCurrent;
    public static InputTouchModeControl instance;
    public SoftTouchButton touchButtonRight;
    public SoftTouchButton touchButtonLeft;
    HeroMover heroMover;
    public Action ActionRightLeftButClick;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        touchButtonRight.ActionButtonFun += RightTapButtonFun;
        touchButtonLeft.ActionButtonFun += LeftTapButtonFun;
        heroMover = ReferenceMaster.instance.heroHealth.GetComponent<HeroMover>();

        //playerConfig.inputTouchModeIndex = 1;
        
        ChangeTouchModeTo(ABManager.GetValueInt("inputTouchModeIndex"));

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeTouchModeTo(int index)
    {
        inputTouchModeCurrent = (InputTouchMode)index; 
        switch (inputTouchModeCurrent)
        {
            case InputTouchMode.Swipe:
                {
                    touchButtonRight.gameObject.SetActive(false);
                    touchButtonLeft.gameObject.SetActive(false);
                    break;
                }
            case InputTouchMode.Tap:
                {
                    touchButtonRight.gameObject.SetActive(true);
                    touchButtonLeft.gameObject.SetActive(true);
                    break;
                }
        }
    }
    public void RightTapButtonFun()
    {
        Debug.Log("RRRRR");
        ActionRightLeftButClick?.Invoke();
        heroMover.InputAtRight();
    }
    public void LeftTapButtonFun()
    {
        Debug.Log("LLLL");
        ActionRightLeftButClick?.Invoke();
        heroMover.InputAtLeft();
    }
    public void EnableTouchThroughTouchMode()
    {
        switch (inputTouchModeCurrent)
        {
            case InputTouchMode.Swipe:
                {
                    InputMouseSystem.instance.TouchEnable();
                    touchButtonRight.gameObject.SetActive(false);
                    touchButtonLeft.gameObject.SetActive(false);
                    break;
                }
            case InputTouchMode.Tap:
                {
                    touchButtonRight.gameObject.SetActive(true);
                    touchButtonLeft.gameObject.SetActive(true);
                    break;
                }
        }
    }
    public void DisableTouchThroughTouchMode()
    {
        touchButtonRight.gameObject.SetActive(false);
        touchButtonLeft.gameObject.SetActive(false);
        InputMouseSystem.instance.TouchDisable();
        //switch (inputTouchModeCurrent)
        //{
        //    case InputTouchMode.Swipe:
        //        {
        //            touchButtonRight.gameObject.SetActive(false);
        //            touchButtonLeft.gameObject.SetActive(false);
        //            break;
        //        }
        //    case InputTouchMode.Tap:
        //        {
        //            touchButtonRight.gameObject.SetActive(true);
        //            touchButtonLeft.gameObject.SetActive(true);
        //            break;
        //        }
        //}
    }
}
