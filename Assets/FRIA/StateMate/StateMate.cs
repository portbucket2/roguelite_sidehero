﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FRIA
{
    public class StateMate<T>: IStateMateAccess<T>
    {
        public bool logStateSwitches;
        public const bool ModuleLog = false;

        public bool IsOnlyDefinedTransitionsAllowed
        {
            get;
            private set;
        }

        private T _current;

        private Dictionary<int, SwitchBoard> switchBoardDictionary = new Dictionary<int, SwitchBoard>();
        //public event System.Action<T, T> onStateSwitch_FromTo;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="initial">initial value</param>
        /// <param name="enableDefinedTransitionOnly">doesnt allow transition if not explicitly defined</param>
        /// <param name="loadEmpties">set all enum values as possible state (might need to add transition)</param>
        public StateMate(T initial, bool enableDefinedTransitionOnly, bool loadEmpties = false)
        {
            _current = initial;
            this.IsOnlyDefinedTransitionsAllowed = enableDefinedTransitionOnly;
            if (loadEmpties)
            {
                if (typeof(T).IsEnum)
                {
                    foreach (var item in System.Enum.GetValues(typeof(T)))
                    {
                        AddEmptyState((int)Convert.ChangeType(item, typeof(int)));
                    }

                }
            }
        }

        public void AddEmptyState(int state)
        {
            SwitchBoard sb = new SwitchBoard();
            switchBoardDictionary.Add(state, sb);
        }
        public void AddStateWithEnableList(T state, List<GameObject> activeList = null)
        {
            int si = (int)Convert.ChangeType(state, typeof(int));

            if (!switchBoardDictionary.ContainsKey(si))
            {
                SwitchBoard sb = new SwitchBoard();
                if (activeList != null) sb.activeList.AddRange(activeList);
                switchBoardDictionary.Add(si, sb);
            }
            else
            {
                SwitchBoard sb = switchBoardDictionary[si];
                if (activeList != null) sb.activeList.AddRange(activeList);
            }

        }
        public void AddTransition(T state, T toState, System.Action onTransition = null)
        {
            int si = (int)Convert.ChangeType(state, typeof(int));
            if (!switchBoardDictionary.ContainsKey(si))
            {
                Debug.LogErrorFormat("{0} state dont exist", state);
            }
            else
            {
                SwitchBoard sb = switchBoardDictionary[si];
                if (!sb.transitions.ContainsKey(toState))
                {
                    TransitionData td = new TransitionData();
                    td.fromState = state;
                    td.toState = toState;
                    td.onTransition += onTransition;
                    sb.transitions.Add(toState, td);
                }
                else
                {
                    Debug.LogErrorFormat("Transition Already Exists {0}=>{1}", state, toState);
                }
            }

        }
        
        public T CurrentState
        {
            get
            {
                return _current;
            }
        }
        public void AddStateEntryCallback(T toState, System.Action onEntry)
        {
            int si = (int)Convert.ChangeType(toState, typeof(int));
            if (!switchBoardDictionary.ContainsKey(si))
            {
                Debug.LogErrorFormat("{0} state dont exist", toState);
                return;
            }
            else
            {
                SwitchBoard sb = switchBoardDictionary[si];
                sb.onEntry += onEntry;
            }
        }
        public void AddStateExitCallback(T fromState, System.Action onExit)
        {
            int si = (int)Convert.ChangeType(fromState, typeof(int));
            if (!switchBoardDictionary.ContainsKey(si))
            {
                Debug.LogErrorFormat("{0} state dont exist", fromState);
                return;
            }
            else
            {
                SwitchBoard sb = switchBoardDictionary[si];
                sb.onExit += onExit;
            }
        }
        public void AddTranstionCallback(T state, T toState, System.Action onTransition)
        {
            int si = (int)Convert.ChangeType(state, typeof(int));
            if (!switchBoardDictionary.ContainsKey(si))
            {
                Debug.LogErrorFormat("{0} state dont exist", state);
                return;
            }
            else
            {
                SwitchBoard sb = switchBoardDictionary[si];
                if (!sb.transitions.ContainsKey(toState))
                {
                    Debug.LogErrorFormat("Transition doesnt Exists {0}=>{1}", state, toState);
                    return;
                }
                else
                {
                    TransitionData td = sb.transitions[toState];
                    td.onTransition += onTransition;
                }
            }
        }

        
        public bool SwitchState(T toState)
        {
            TransitionData td = null;
            int ci = (int)Convert.ChangeType(CurrentState, typeof(int));
            SwitchBoard oldSwitchBoard = switchBoardDictionary[ci];

            if (IsOnlyDefinedTransitionsAllowed)
            {
                if (!oldSwitchBoard.transitions.ContainsKey(toState))
                {

                    if (ModuleLog) Debug.LogFormat("Transition Denied! {0}=>{1}", CurrentState, toState);
                    return false;
                }
            }

            if (oldSwitchBoard.transitions.ContainsKey(toState))
            {
                td = oldSwitchBoard.transitions[toState];
            }

            T prev = _current;
            _current = toState;

            int to_index = (int)Convert.ChangeType(toState, typeof(int));



            foreach (KeyValuePair<int, SwitchBoard> kvp in switchBoardDictionary)
            {
                if (kvp.Key != to_index)
                {
                    foreach (var gameobject in kvp.Value.activeList)
                    {
                        gameobject.SetActive(false);
                    }
                }
            }
            foreach (var item in switchBoardDictionary[to_index].activeList)
            {
                item.SetActive(true);
            }
            oldSwitchBoard.onExit?.Invoke();
            if (td != null)
            {
                td.onTransition?.Invoke();
            }
            //onStateSwitch_FromTo?.Invoke(prev, toState);
            if (logStateSwitches) Debug.LogFormat("Switching from {0} to {1}", prev, toState);
            SwitchBoard newSwitchboard = switchBoardDictionary[to_index];
            newSwitchboard.onEntry?.Invoke();

            return true;
        }
        private class SwitchBoard
        {
            public List<GameObject> activeList = new List<GameObject>();
            public Dictionary<T, TransitionData> transitions = new Dictionary<T, TransitionData>();
            public System.Action onEntry;
            public System.Action onExit;
        }
        public class TransitionData
        {
            public T fromState;
            public T toState;
            public System.Action onTransition;
        }
    }

    public interface IStateMateAccess<T>
    {
        T CurrentState { get; }
        void AddStateEntryCallback(T toState, System.Action onEntry);
        void AddStateExitCallback(T fromState, System.Action onExit);
        void AddTranstionCallback(T state, T toState, System.Action onTransition);
    }
}