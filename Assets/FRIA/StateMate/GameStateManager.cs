﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class GameStateManager : MonoBehaviour
{
    public static GameStateManager Instance { get; private set; }
    public static IStateMateAccess<GameState> stateAccess { get { return Instance.stateMate; } }
    public static GameState State { get { return stateAccess.CurrentState; } }
    public static bool inAction { get { return State == GameState.Action; } }
    
    private StateMate<GameState> stateMate;
    private void Awake()
    {
        Instance = this;
        stateMate = new StateMate<GameState>(initial: GameState.Init, enableDefinedTransitionOnly: true, loadEmpties: true);

        stateMate.AddTransition(GameState.Init, GameState.Upgrade);
        stateMate.AddTransition(GameState.Upgrade, GameState.Init);
        stateMate.AddTransition(GameState.Init, GameState.Action);
        stateMate.AddTransition(GameState.Action, GameState.Fail);
        stateMate.AddTransition(GameState.Fail, GameState.Action);
        stateMate.AddTransition(GameState.Action, GameState.Success);
        stateMate.logStateSwitches = true;

    }
    public static void RequestSwitch(GameState gs)
    {
        Instance.stateMate.SwitchState(gs);
    }
}

public enum GameState
{
    Init = 0,
    Upgrade = 1,
    Action = 2,
    Fail = 3,
    Success = 4, 
}