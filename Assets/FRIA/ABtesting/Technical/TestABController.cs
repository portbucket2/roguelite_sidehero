﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
public class TestABController : MonoBehaviour
{
    public GameObject root;

    public Button applyButton;
    // Start is called before the first frame update
    public static bool isAB_trialValueSetupPending = false;
    IEnumerator Start()
    {
        root.SetActive(false);
        if (modules.Count > 0)
        {
            isAB_trialValueSetupPending = true;
            //while (!ABManager.isDataFetchComplete)
            //{
                yield return null;
            //}
            root.SetActive(true);
            LoadModules();
            applyButton.onClick.AddListener(OnApply);
            for (int i = modules.Count - 1; i >= 0; i--)
            {
                if (!modules[i].loaded)
                {
                    modules[i].gameObject.SetActive(false);
                    modules.RemoveAt(i);
                }
            }
            if (modules.Count == 0) OnApply();
        }

    }
    public List<TestABDataChangeUI> modules;
    void LoadModules()
    {
        TypeInfo tpi = typeof(ABManager.PlayerConfig).GetTypeInfo();

        FieldInfo[] finfos = tpi.GetFields();
        for (int i = 0; i < finfos.Length; i++)
        {
            FieldInfo fi = finfos[i];

            if (i >= modules.Count) break;
            else
            {
                switch (fi.Name)
                {
                    default:
                        {
                            modules[i].Load(fi.Name);
                        }
                        break;
                    case "inputTouchModeIndex":
                        {
                            modules[i].Load(fi.Name,
                                new string[] { "0", "1"},
                                new string[] { "Swipe", "Tap" }
                                );
                        }
                        break;
                    case "example1_valuesonly":
                        {
                            modules[i].Load(fi.Name, 
                                new string[] {"0","1","2"});
                        }
                        break;
                    case "example2_value_and_title":
                        {
                            modules[i].Load(fi.Name, 
                                new string[] { "0", "1", "2" },
                                new string[] { "knife", "peeler", "lightsaber" }
                                );
                        }
                        break;
                }
            }
        }
    }






    void OnApply()
    {
        foreach (var item in modules)
        {
            if (item.loaded) item.OnApply();
        }
        root.SetActive(false);
        isAB_trialValueSetupPending = false;
    }

}
