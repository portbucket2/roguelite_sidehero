﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace FRIA
{

    public class EditorUtilities
    {
        [UnityEditor.MenuItem("Potato/All Player Prefs")]
        public static void ClearPP()
        {
            PlayerPrefs.DeleteAll();
        }
        
        //[UnityEditor.MenuItem("Clean/BMAD PData/Goals")]
        //public static void ClearGoalProgress()

        //{
        //    PlayerPrefs.DeleteKey("GOALSET_INDEX");
        //    PlayerPrefs.DeleteKey("GOAL_STATE_0");
        //    PlayerPrefs.DeleteKey("GOAL_STATE_1");
        //    PlayerPrefs.DeleteKey("GOAL_STATE_2");
        //}

        //[UnityEditor.MenuItem("Clean/Daily Reward Timer")]
        //public static void ClearDailyRewardTimer()

        //{
        //    PlayerPrefs.DeleteKey("LAST_CLAIM_DATE_DAILYREWARD");
        //}


        [UnityEditor.MenuItem("Potato/Generate Conflict")]
        public static void GenerateConflict()
        {
            string path = "ConflictForGreaterGood.txt";

            string data = string.Format("GitConflictGeneration: {0}\n{1} - Device Name: {2}\n{3}\n{4}", System.DateTime.Now,System.DateTime.Now.Ticks,SystemInfo.deviceName, SystemInfo.deviceModel, SystemInfo.deviceUniqueIdentifier);
            StreamWriter sw = File.CreateText(path);
            sw.WriteLine(data);
            sw.Close();
            Debug.LogErrorFormat(data);
        }

    }
}