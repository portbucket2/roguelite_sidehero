﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponThrowable : MonoBehaviour
{
    public bool running;
    public float runSpeed;
    float damage;
    public GameObject visual;
    bool collided;
    public Collider trigger;
    public EnemyAttackSystem enemyAttackSystem;
    // Start is called before the first frame update
    void Start()
    {
        ThrowMe();
    }

    // Update is called once per frame
    void Update()
    {
        if (running)
        {
            transform.Translate(0, 0, runSpeed * Time.deltaTime);
            visual.transform.localRotation *= Quaternion.Euler(new Vector3(runSpeed *300* Time.deltaTime, 0, 0));
        }
    }
    public void ThrowMe()
    {
        running = true;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().useGravity = false;
        Destroy(this.gameObject,5f);
        trigger.isTrigger = true;

        if(enemyAttackSystem != null)
        {
            damage = enemyAttackSystem.enemyDamage;
        }
    }
    
    public void CollideMe()
    {
        if (collided)
        {
            return;
        }
        running = false;
        Rigidbody r = GetComponent<Rigidbody>();
        r.isKinematic = false;
        r.useGravity = true;

        r.AddForce(transform.up *1000 + transform.forward * 500);
        r.AddTorque(transform.right * 1000);
        trigger.isTrigger = false;
        collided = true;
        Destroy(this.gameObject,2f);
    }
    public void DestroyWeapon()
    {

        Destroy(this.gameObject);

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 9)
        {
            HeroAttackState heroAttackState = other.GetComponent<HeroAttackDodge>().heroAttackState;
            if(UiManage.instance.GetLevelCompletionValue() < 1)
            {
                other.GetComponent<HeroHealth>().DamageHeathBy(damage);
            }
            
            DestroyWeapon();
            return;
            switch (heroAttackState)
            {


                case HeroAttackState.HeroIdle:
                    {
                        other.GetComponent<HeroHealth>().DamageHeathBy(damage);
                        DestroyWeapon();
                        break;
                    }
                case HeroAttackState.HeroAttacking:
                    {
                        CollideMe();
                        other.GetComponent<HeroMover>().StopMoving();
                        

                        break;
                    }
            }

            
        }
    }
}
