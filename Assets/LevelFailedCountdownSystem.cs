﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using APSdk;

public class LevelFailedCountdownSystem : MonoBehaviour
{
    public float reviveDuration;
    public float timer;
    bool timerRunning;
    public Text textTimer;
    public Image imageRoundFill;
    public Button buttonRevive;
    public int levelIndexToRevive;
    public static LevelFailedCountdownSystem instance;
    public Action actionReviveHero;

    public bool reviveRvButtonClicked;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        ReferenceMaster.instance.heroHealth.actionHeroDeath += ReviveLevelValues;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerRunning)
        {
            timer -= Time.deltaTime;
            if(timer<= 0)
            {
                PauseRevive();
                NoThanks();
            }
            UpdateUiVisual();
        }
    }
    public void StartRevive()
    {
        timerRunning = true;
        timer = reviveDuration;
        buttonRevive.gameObject.SetActive(true);

        
    }
    public void StopRevive()
    {
        timerRunning = false;
        timer = 0;
        buttonRevive.gameObject.SetActive(false);
    }
    public void PauseRevive()
    {
        timerRunning = false;
        //timer = 0;
        //buttonRevive.gameObject.SetActive(false);
    }
    public void UpdateUiVisual()
    {
        textTimer.text = "" + (int)(timer + 0.5f);
        imageRoundFill.fillAmount = (timer / reviveDuration);
    }
    public void OnDisable()
    {
        StopRevive();
    }
    public void ButtonReviveFun()
    {
        //GameManagementMain.instance.LevelUpdateTo(levelIndexToRevive);
        //
        //ReferenceMaster.instance.heroHealth.HeroHealthRevive();
        //EnemySpawner.instance.ReviveAllEnemiesAttack();
        //StopRevive();
        //GameStatesControl.instance.ChangeGameStateTo(GameStates.GamePlay);
        //InputMouseSystem.instance.TouchEnable();
        //UiManage.instance.GamePlayPanelAppear();
        //ReferenceMaster.instance.heroHealth.GetComponent<HeroShieldSystem>().ShieldReviveEnable(5);
        //
        //actionReviveHero?.Invoke();

        //APLionKitWrapper.Instance.RewardedAd.ShowRewardedAd(ReviveNow, NoThanks, NoThanks, GameManagementMain.instance.levelIndexDisplayed);

    }
    public void ButtonReviveRVCall()
    {
        PauseRevive();
        //GameManagementMain.instance.AnalyticsCallOnRvForRevive("ReviveRV");

        int level = levelIndexToRevive + 1;
        //APSdkLogger.Log("(Before) RVCallForRevive = " + level);
        APLionKitWrapper.Instance.RewardedAd.ShowRewardedAd(
            "ReviveRV",
            "level",
            level,
            ReviveNow,
            NoThanks,
            ReviveRVCloseFun);
    }

    public void ReviveLevelValues()
    {
        levelIndexToRevive = GameManagementMain.instance.levelIndexDisplayed;
        int levelIndex = AreaSelectionUiManage.instance.areaUiPrefabs[AreaSelectionUiManage.instance.currentAreaIndex].GetComponent<AreaUI>().minLevelIndex;

        GameManagementMain.instance.LevelUpdateTo(levelIndex);
    }

    public void ReviveNow()
    {
        reviveRvButtonClicked = true;
        
    }
    public void NoThanks()
    {
        
        Debug.Log("NO THANKS CALL WORKING");
        APLionKitWrapper.Instance.InterstitialAd.ShowInterstitialAd(InterStitialAdSuccess, InterStitialAdSuccess);
        
    }
    public void InterStitialAdSuccess()
    {
        StopRevive();
        UiManage.instance.RestartButtonFun();
        
    }
    //public void NoThanksRvFailed()
    //{
    //    Debug.Log("NO THANKS CALL WORKING");
    //    UiManage.instance.RestartButtonFun();
    //    StopRevive();
    //}
    public void ReviveRVCloseFun()
    {
        if (reviveRvButtonClicked)
        {
            Debug.Log("REVIVE CALL WORKING");

            GameManagementMain.instance.LevelUpdateTo(levelIndexToRevive);

            ReferenceMaster.instance.heroHealth.HeroHealthRevive();
            EnemySpawner.instance.ReviveAllEnemiesAttack();
            StopRevive();
            GameStatesControl.instance.ChangeGameStateTo(GameStates.GamePlay);
            InputTouchModeControl.instance.EnableTouchThroughTouchMode();
            UiManage.instance.GamePlayPanelAppear();
            ReferenceMaster.instance.heroHealth.GetComponent<HeroShieldSystem>().ShieldReviveEnable(5);

            actionReviveHero?.Invoke();

            //GameManagementMain.instance.AnalyticsCallOnRvForRevive("ReviveRV");
        }
        else
        {
            NoThanks();
        }
        reviveRvButtonClicked = false;
    }
}
