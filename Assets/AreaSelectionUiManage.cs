﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AreaSelectionUiManage : MonoBehaviour
{
    public GameObject areasHolder;
    public Vector3 rootPosAreasHolder;
    public GameObject[] areaUiPrefabs;
    public int currentAreaIndex;
    public int lastUnlockedAreaIndex;
    public bool snapping;
    public GameObject startButton;
    public GameObject lockedText;
    public Action actionAreaUiSnap;
    public Action actionAreaStartButton;

    public static AreaSelectionUiManage instance;

    private void Awake()
    {
        instance = this;
        PositionTheAreas();
    }
    // Start is called before the first frame update
    void Start()
    {
        //PositionTheAreas();
        UiMouseInputs.insatnce.actionSwipeLeft += SnapToNextArea;
        UiMouseInputs.insatnce.actionSwipeRight += SnapToPrevArea;

        actionAreaUiSnap += EvaluateStartButton;

       //GameStatesControl.instance.actionMenuIdle += DetermineAreaAtMenuUiState;
       //DetermineAreaAtMenuUiState();
    }

    // Update is called once per frame
    void Update()
    {
        if (snapping)
        {
            areasHolder.transform.position = Vector3.Lerp(areasHolder.transform.position, rootPosAreasHolder+ new Vector3(Screen.width * -currentAreaIndex, 0, 0), Time.deltaTime * 10);
            if(Vector3.Distance(areasHolder.transform.position,  rootPosAreasHolder + new Vector3(Screen.width * -currentAreaIndex, 0, 0))< 10f)
            {
                areasHolder.transform.position = rootPosAreasHolder+new Vector3(Screen.width * -currentAreaIndex, 0, 0);
                snapping = false;

                actionAreaUiSnap?.Invoke();
            }
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            //SnapToNextArea();
            SnapToAreaIndex(2);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            //SnapToPrevArea();
            SnapToAreaIndex(0);
        }
    }
    public void SnapToNextArea()
    {
        if(GameStatesControl.instance.GetCurrentGameState() != GameStates.MenuIdle)
        {
            return;
        }
        if (currentAreaIndex >= areaUiPrefabs.Length - 1)
        {
            return;
        }
        currentAreaIndex += 1;
        snapping = true;

    }
    public void SnapToPrevArea()
    {
        if (GameStatesControl.instance.GetCurrentGameState() != GameStates.MenuIdle)
        {
            return;
        }
        if (currentAreaIndex <= 0)
        {
            return;
        }
        currentAreaIndex -= 1;
        snapping = true;

    }
    public void SnapToAreaIndex(int index, bool instant = false)
    {
        currentAreaIndex = index;
        if (currentAreaIndex < 0)
        {
            return;
        }
        if (instant)
        {
            areasHolder.transform.position = rootPosAreasHolder + new Vector3(Screen.width * -currentAreaIndex, 0, 0);
            actionAreaUiSnap?.Invoke();
        }
        else
        {
            snapping = true;
        }
        
        
    }

    public void PositionTheAreas()
    {
        for (int i = 0; i < areaUiPrefabs.Length; i++)
        {
            areaUiPrefabs[i].transform.localPosition = Vector3.zero;
            areaUiPrefabs[i].transform.position += new Vector3(Screen.width * i , 0, 0);
        }
        rootPosAreasHolder = areasHolder.transform.position;
    }
    public void EvaluateStartButton()
    {
        if(currentAreaIndex<= lastUnlockedAreaIndex)
        {
            startButton.SetActive(true);
            lockedText.SetActive(false);
        }
        else
        {
            startButton.SetActive(false);
            lockedText.SetActive(true);
        }

        for (int i = 0; i < areaUiPrefabs.Length; i++)
        {
            areaUiPrefabs[i].GetComponent<AreaUI>().EvaluateLockedVisual();
        }
    }
    public void DetermineAreaAtMenuUiState()
    {
        lastUnlockedAreaIndex = PlayerPrefs.GetInt("lastUnlockedAreaIndex", 0);

        Debug.Log("WORKING");
        //PositionTheAreas();
        int currentAreaIndex = 0;
        for (int i = 0; i < areaUiPrefabs.Length; i++)
        {
            if (GameManagementMain.instance.levelIndexDisplayed >= areaUiPrefabs[i].GetComponent<AreaUI>().minLevelIndex)
            {
                currentAreaIndex = i;
                if(currentAreaIndex > lastUnlockedAreaIndex)
                {
                    lastUnlockedAreaIndex = currentAreaIndex;
                    PlayerPrefs.SetInt("lastUnlockedAreaIndex", lastUnlockedAreaIndex);
                }
                
            }
        }
        SnapToAreaIndex(currentAreaIndex, true);

        EvaluateStartButton();
    }
    public bool EvaluateLastLevelOfThisAreaOrNot()
    {
        bool r = false;
        if(areaUiPrefabs[currentAreaIndex].GetComponent<AreaUI>().maxLevelIndex == GameManagementMain.instance.levelIndexDisplayed)
        {
            r = true;
        }
        //DoubleCheckCorrection
        for (int i = 0; i < areaUiPrefabs.Length; i++)
        {
            if (areaUiPrefabs[i].GetComponent<AreaUI>().maxLevelIndex == GameManagementMain.instance.levelIndexDisplayed)
            {
                r = true;
                break;
            }
        }

        //Debug.Log("CHOORR " + r +" "+ areaUiPrefabs[currentAreaIndex].GetComponent<AreaUI>().maxLevelIndex+" " + GameManagementMain.instance.levelIndexDisplayed);
        return r;
    }
    public void DetermineLevelIndexAccordingToFirstLevelOfArea()
    {
        GameManagementMain.instance.LevelUpdateTo(areaUiPrefabs[currentAreaIndex].GetComponent<AreaUI>().minLevelIndex);
        actionAreaStartButton?.Invoke();
    }
    
    
}
