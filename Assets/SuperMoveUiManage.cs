﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuperMoveUiManage : MonoBehaviour
{
    public GameObject buttonSuper;
    public GameObject parent;
    public Image fillBarSuper;
    public float superFill;
    public bool superUiEnabled;
    public float fillSpeed;
    public Image superIconFadedImage;
    public Image superIconButtonImage;
    //public static SuperMoveUiManage instance;
    public float fillIncreaseOnHit;
    
    public float fillIncreaseOnHitApp
    {
        get
        {
            if (AbilityCardMultipliers.instance != null)
            {
                return ((float)fillIncreaseOnHit * AbilityCardMultipliers.instance.superMoveRechargeMul);
            }
            else
            {
                return fillIncreaseOnHit;
            }

        }
    }


    private void Awake()
    {
        //instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionMenuIdle += SuperUiDisable;
        GameStatesControl.instance.actionGameStart += SuperUiDisable;
        GameStatesControl.instance.actionGamePlay += SuperUiEnable;
        GameStatesControl.instance.actionLevelResult += SuperUiDisable;

        SuperUiDisable();
    }

    // Update is called once per frame
    void Update()
    {
        FillSuper();
    }
    public void FillSuperBy(float f)
    {
        if (ReferenceMaster.instance.heroHealth.superMoveMode)
        {
            return;
        }
        if (!SuperPowersUnlockSystem.instance.superMoveUnlocked)
        {
            
            return;
        }
        superFill += f;
        if (superFill > 100)
        {
            superFill = 100;
            buttonSuper.SetActive(true);
        }
        SuperUiValuesUpdate();
    }
    
    public void FillSuper()
    {
        if (!superUiEnabled)
        {
            return;
        }
        if(fillSpeed <= 0)
        {
            //return;
        }
        if(superFill < 100)
        {
            superFill += Time.deltaTime * fillSpeed;
        }
        else
        {
            superFill = 100;
            buttonSuper.SetActive(true);
        }
        SuperUiValuesUpdate();
        
    }
    public void SuperUiEnable()
    {
        if (!SuperPowersUnlockSystem.instance.superMoveUnlocked)
        {
            SuperUiDisable();
            return;
        }
        UpdateSuperIconInGameplay();
        superUiEnabled = true;
        //buttonSuper.SetActive(true);
        fillBarSuper.gameObject.SetActive(true);
        parent.SetActive(true);
    }
    public void SuperUiDisable()
    {
        superUiEnabled = false;
        buttonSuper.SetActive(false);
        fillBarSuper.gameObject.SetActive(false);
        parent.SetActive(false);
    }
    public void SuperApplyAndUiReset()
    {
        if (ReferenceMaster.instance.heroHealth.HeroDeadOrnot())
        {
            return;
        }
        ReferenceMaster.instance.heroHealth.GetComponent<HeroAttackDodge>().SuperMoveEnable();
        superFill = 0;
        buttonSuper.SetActive(false);
    }
    public void UpdateSuperIconInGameplay()
    {
        superIconFadedImage.sprite = SuperPowersUnlockSystem.instance.superMoveCurrent.superIconImageSprite;
        superIconButtonImage.sprite = SuperPowersUnlockSystem.instance.superMoveCurrent.superIconImageSprite;
    }
    public void SuperUiValuesUpdate()
    {
        fillBarSuper.fillAmount = superFill * 0.01f;
    }
    private void OnEnable()
    {
        SuperUiValuesUpdate();
    }
    //Hard Coded Fun
    public void SpecialSuperFillFull()
    {
        superFill = 100;
        buttonSuper.SetActive(true);
        SuperUiValuesUpdate();
    }
}
