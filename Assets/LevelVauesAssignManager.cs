﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelVauesAssignManager : MonoBehaviour
{
    public List<LevelValues> levelValues;
    [System.Serializable]
    public class LevelValues
    {
        public string title;
        public List<GameObject> enemyListInLevel;
        public List<float> enemyAppearIntervals;
        public List<FacingMode> enemySpawnDirection;
        public List<bool> pauseSpawn;
        public bool hasBossFight;
        public List<GameObject> bossListInLevel;
       
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
