﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponThrower : MonoBehaviour
{
    public GameObject weaponObj;
    public GameObject weaponRootRef;
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<AnimationEventsManager>().actionEnemyThrowWeapon += ThrowWeapon;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ThrowWeapon()
    {
        GameObject go = Instantiate(weaponObj, weaponRootRef.transform.position, weaponRootRef.transform.rotation);
        go.GetComponent<WeaponThrowable>().enemyAttackSystem = GetComponent<EnemyAttackSystem>();
    }
}
