﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroShieldSystem : MonoBehaviour
{
    [SerializeField]
    private bool shieldOn;
    public float shieldHealthMax;
    [SerializeField]
    float shieldHealth;
    public GameObject shieldVisual;
    bool durationMode;
    public bool shieldPending;
    // Start is called before the first frame update
    void Start()
    {
        //EnableShield();
        GameStatesControl.instance.actionLevelStart += ShieldPendingCheckEnableShield;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableShield()
    {
        DisableShield();
        StopAllCoroutines();
        shieldOn = true;
        shieldHealth = shieldHealthMax;
        shieldVisual.SetActive(true);
    }
    public void DisableShield()
    {
        shieldOn = false;
        durationMode = false;
        shieldVisual.SetActive(false);
    }
    public void DamageShiledBy(float d)
    {
        if (durationMode)
        {
            return;
        }
        shieldHealth -= d;
        shieldHealth = Mathf.Clamp(shieldHealth, 0, shieldHealthMax);
        if(shieldHealth <= 0)
        {
            DisableShield();
        }
    }
    public bool ShieldEnabledOnNot()
    {
        return shieldOn;
    }
    IEnumerator ShieldForSomeMoments(float t)
    {
        yield return new WaitForSeconds(t);
        DisableShield();
    }
    public void ShieldReviveEnable(float t)
    {
        StopAllCoroutines();
        shieldOn = true;
        durationMode = true;
        shieldVisual.SetActive(true);
        StartCoroutine(ShieldForSomeMoments(t));
    }
    public void ShieldPendingCheckEnableShield()
    {
        if (shieldPending)
        {
            ShieldReviveEnable(7);
            shieldPending = false;
        }
    }
    public void ShieldPendindEnable()
    {
        shieldPending = true;
    }
}
