﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaUI : MonoBehaviour
{
    public int areaIndex;
    public int minLevelIndex;
    public int maxLevelIndex;
    public Text title;
    public Text levelRange;
    public Image imageLocked; 
    // Start is called before the first frame update
    void Start()
    {
        title.text = "AREA " + (areaIndex + 1);
        levelRange.text = "LEVEL " + (minLevelIndex+1) + " - " + (maxLevelIndex+1);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LockedVisualShow()
    {
        imageLocked.gameObject.SetActive(true);
    }
    public void UnlockedVisualShow()
    {
        imageLocked.gameObject.SetActive(false);
    }
    public void EvaluateLockedVisual()
    {
        Debug.Log("EEEE " + areaIndex + " " + AreaSelectionUiManage.instance.lastUnlockedAreaIndex);
        if (areaIndex <= AreaSelectionUiManage.instance.lastUnlockedAreaIndex)
        {

            UnlockedVisualShow();
        }
        else
        {
            LockedVisualShow();
        }
    }
}
