﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeroAttackDodge : MonoBehaviour
{
    public ParticleSystem HeroAttackParticle;
    public float heroAttackRange;
    public EnemyHealth enemyHealthInRange;
    public float hitPower;
    HeroWeaponSystem heroWeaponSystem;
    public Action actionHeroDamageEnemy;
    public float hitPowerApp
    {
        get
        {
            if (AbilityCardMultipliers.instance != null)
            {
                //return ((float)(hitPower + GetComponent<HeroWeaponSystem>().addedDamageForWeaponApp) * AbilityCardMultipliers.instance.heroHitPowerMul);
                return ((float)((hitPower * AbilityCardMultipliers.instance.heroHitPowerMul)*(1 + (GetComponent<HeroWeaponSystem>().addedDamagePercentageForWeaponApp* 0.01f) )));
            }
            else
            {
                return hitPower;
            }

        }
    }

    public ParticleSystem particleSuper;

    public HeroAttackState heroAttackState;
    public bool canDoDamage;
    public bool attackInputOverride;
    // Start is called before the first frame update
    void Start()
    {
        heroWeaponSystem = GetComponent<HeroWeaponSystem>();

        GetComponentInChildren<AnimationEventsManager>().actionHeroSuperMove += HeroSuperMoveApply;
        //GetComponentInChildren<AnimationEventsManager>().actionHeroSuperMove += HeroSuperMoveParticleFun;
        GetComponentInChildren<AnimationEventsManager>().actionHeroSuperMoveDisable += SuperMoveDisable;
        GetComponentInChildren<AnimationEventsManager>().actionHeroHitEnemy += HeroAttackDamage;
        GetComponentInChildren<AnimationEventsManager>().actionHeroAttackEnd += HeroAttackEnd;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SuperMoveEnable();
        }
    }

    public void HeroAttack()
    {
        HeroAttackParticle.Play();

        //switch (heroWeaponSystem.heroWeaponCurrent)
        //{
        //    case HeroWeapons.None:
        //        {
        //            ReferenceMaster.instance.heroAnimControl.AttackAnimPlay();
        //            break;
        //        }
        //    case HeroWeapons.Shield:
        //        {
        //            if (heroWeaponSystem.WeaponThrowOrNot())
        //            {
        //                ReferenceMaster.instance.heroAnimControl.GoHeroWeaponThrow();
        //            }
        //            else
        //            {
        //                ReferenceMaster.instance.heroAnimControl.AttackAnimPlay();
        //            }
        //            break;
        //        }
        //    case HeroWeapons.Hammer:
        //        {
        //            if (heroWeaponSystem.WeaponThrowOrNot())
        //            {
        //                ReferenceMaster.instance.heroAnimControl.GoHeroWeaponThrow();
        //            }
        //            else
        //            {
        //                ReferenceMaster.instance.heroAnimControl.AttackAnimPlay();
        //            }
        //            break;
        //        }
        //}
        ReferenceMaster.instance.heroAnimControl.AttackAnimPlay();
        ChangeHeroAttackStateTo(HeroAttackState.HeroAttacking);

        EnableHeroDamageability();
        
        //StartCoroutine(HeroAttackDamageCoroutine(0.1f));
        //HeroAttackDamage();

    }
    public void HeroDodge()
    {

    }
    IEnumerator HeroAttackDamageCoroutine(float d = 0)
    {
        yield return new WaitForSeconds(d);
        HeroAttackDamage();
    }
    public void HeroAttackDamage()
    {
        if (!canDoDamage)
        {
            //Debug.Log("Damage Blocked");
            return;
        }
        if (enemyHealthInRange != null)
        {
            enemyHealthInRange.DamageHeathBy(hitPowerApp);
            actionHeroDamageEnemy?.Invoke();
            CameraShakeHandler.instance.Swing();
            //Debug.Log("Hit PowerApp "+ hitPowerApp);
        }
        
    }
    public void HeroSuperMoveApply()
    {
        EnemySpawner.instance.SuperMoveDamage();
        CameraShakeHandler.instance.Swing(1.0f,3);
        //particleSuper.Play();

        //Invoke("SuperMoveDisable", 1);
        GameManagementMain.instance.AnalyticsSuperMoveUse(SuperPowersUnlockSystem.instance.superMoveCurrent.title);
    }
    public void HeroSuperMoveParticleFun()
    {
        int a = SuperPowersUnlockSystem.instance.superMoveCurrent.superIndex;
        switch (a)
        {
            case 0:
                {
                    particleSuper.Play();
                    break;
                }
            case 1:
                {
                    break;
                }
            case 2:
                {
                    break;
                }
            case 3:
                {
                    break;
                }
        }
    }
    public void SuperMoveEnable()
    {
        
        GetComponent<HeroHealth>(). superMoveMode = true;
        if (SuperPowersUnlockSystem.instance.superMoveUnlocked)
        {
            ReferenceMaster.instance.heroAnimControl.GoSuperMove(SuperPowersUnlockSystem.instance.superMoveCurrent.trigger);
        }
        else
        {
            ReferenceMaster.instance.heroAnimControl.GoSuperMove();
        }
        
    }
    public void SuperMoveDisable()
    {
        GetComponent<HeroHealth>().superMoveMode = false;
    }
    public void ChangeHeroAttackStateTo(HeroAttackState h)
    {
        heroAttackState = h;
        switch (heroAttackState)
        {
            case HeroAttackState.HeroIdle:
                {
                    break;
                }
            case HeroAttackState.HeroAttacking:
                {
                    break;
                }
        }
    }
    public void HeroAttackEnd()
    {
        DisableHeroDamageability();
        ChangeHeroAttackStateTo(HeroAttackState.HeroIdle);
    }
    public void EnableHeroDamageability()
    {
        if (canDoDamage)
        {
            attackInputOverride = true;
        }
        canDoDamage = true;
    }
    public void DisableHeroDamageability()
    {
        if (attackInputOverride)
        {
            attackInputOverride = false;
        }
        else
        {
            canDoDamage = false;
        }
        
        
    }
    
}
