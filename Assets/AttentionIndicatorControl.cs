﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttentionIndicatorControl : MonoBehaviour
{
    public GameObject indicatorRight;
    public GameObject indicatorLeft;
    public static AttentionIndicatorControl instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CheckIndicator();
        GameStatesControl.instance.actionLevelEnd += CheckIndicator;
        GameStatesControl.instance.actionLevelResult += CheckIndicator;
        //GameStatesControl.instance.actionMenuIdle += CheckIndicator;
        GameStatesControl.instance.actionGameStart += CheckIndicator;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CheckIndicator()
    {
        bool r = false;
        bool l = false;
        for (int i = 0; i < LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count; i++)
        {
            if (!LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyAttackSystem>().attackLoopRunning)
            {
                if(LevelValuesCurrent.instance.enemyAliveCurrentLevel[i].GetComponent<EnemyAnimControl>().faceModeMul == 1)
                {
                    l = true;
                }
                else
                {
                    r = true;
                }
            }
        }

        if (r)
        {
            indicatorRight.SetActive(true);
        }
        else
        {
            indicatorRight.SetActive(false);
        }
        if (l)
        {
            indicatorLeft.SetActive(true);
        }
        else
        {
            indicatorLeft.SetActive(false);
        }
    }
}
