﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExplosiveThrowable : MonoBehaviour
{
    public GameObject throwableObj;
    public GameObject root;
    public float throwLifetime;
    float timer;
    public bool thrown;
    public float throwDis;
    public float addedMaxHeight;
    public Vector3 oldPos;
    public Vector3 newPos;
    public float progression;
    public Vector3 curPos;
    public AnimationCurve yCurve;
    public Action actionOnExplosion;
    public float yVel;
    public float g;
    public Vector3 throwVel;
    
    public GameObject redZone;
    public EnemyAttackSystem enemyAttackSystem;
    public float explosionRange;
    public ParticleSystem explosiveParticle;
    public ParticleSystem muzzleParticle;
    public AnimationEventsManager animationEventsManager;
    //public Vector3 rootPos;
    // Start is called before the first frame update
    void Start()
    {
        //throwableObj.transform.localPosition = rootPos;
        //root.transform.position = throwableObj.transform.position;
        //ResetThrowable();
        ResetThrowable();
        enemyAttackSystem = GetComponentInParent<EnemyAttackSystem>();
        animationEventsManager = GetComponentInParent<EnemyAttackSystem>().GetComponentInChildren<AnimationEventsManager>();
        if(animationEventsManager != null)
        {
            animationEventsManager.actionEnemyThrowThrowable += ThrowIt;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            ThrowIt();
            //ThrowItRb();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            //ThrowIt();
            //ResetThrowable();
            RedZoneIndicate(3);
        }
        if (thrown)
        {
            
            if(timer < throwLifetime)
            {
                timer += Time.deltaTime;
            }
            else
            {
                timer = throwLifetime;
                thrown = false;
                actionOnExplosion?.Invoke();
                ResetThrowable();
                HitPlayer();
            }
            progression = timer / throwLifetime;
            curPos = Vector3.Lerp(oldPos, newPos, progression) + new Vector3(0, (yCurve.Evaluate(progression)*addedMaxHeight)- (Mathf.Lerp(yCurve.Evaluate(0),0, progression) * addedMaxHeight),0);
            //curPos.z = Mathf.Lerp(oldPos.z, newPos.z, progression);
            //curPos.y += yVel;
            //yVel -= Time.deltaTime*g;
            throwableObj.transform.localPosition = curPos;
            throwableObj.transform.Rotate(1000 * Time.deltaTime, 0, 0);
            //curPos.y = 
        }
    }
    public void ThrowIt()
    {
       thrown = true;
       oldPos = throwableObj.transform.localPosition;
       newPos =  new Vector3(0,0,throwDis);
       newPos.y = 0;
       curPos = oldPos;
       //g = yVel * 0.0565f / 0.05f;
       
       float y = oldPos.y;

        throwableObj.SetActive(true);

        if(muzzleParticle != null)
        {
            muzzleParticle.Play();
        }
        //yVel = (-y - (0.5f * g * throwLifetime * throwLifetime)) / throwLifetime;

    }
    //public void ThrowItRb()
    //{
    //    //throwableObj.AddComponent<Rigidbody>();
    //    if (thrown)
    //    {
    //        return;
    //    }
    //    throwableObj.SetActive(true);
    //    thrown = true;
    //    throwableObj.GetComponent<Rigidbody>().velocity = (transform.forward * throwVel.z) + (transform.up*throwVel.y);
    //    throwableObj.GetComponent<Rigidbody>().AddTorque(transform.right * 50f);
    //}
    public void ResetThrowableRb()
    {
        throwableObj.GetComponent<Rigidbody>().velocity = Vector3.zero;
        throwableObj.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        
        throwableObj.transform.localPosition = root.transform.localPosition;
        throwableObj.SetActive(false);

        thrown = false;
    }
    public void ResetThrowable()
    {
        
        throwableObj.SetActive(false);

        thrown = false;
        timer = 0;
        redZone.SetActive(false); 
        throwableObj.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
    public void RedZoneIndicate(float maxRange)
    {
        
        
        redZone.SetActive(true);
        redZone.transform.position = ReferenceMaster.instance.heroHealth.transform.position;
        throwDis = redZone.transform.localPosition.z;
        throwDis = Mathf.Clamp(throwDis, 0, maxRange);
        redZone.transform.localPosition = new Vector3(0, 0, throwDis);

        enemyAttackSystem. waitForExplosion = true;
    }
    public void RedZoneClear()
    {
        ResetThrowable();
        enemyAttackSystem.waitForExplosion = false;
    }
    public void HitPlayer()
    {
        if(enemyAttackSystem != null)
        {
            if (Vector3.Distance(redZone. transform.position, ReferenceMaster.instance.heroHealth.transform.position) <= explosionRange)
            {
                ReferenceMaster.instance.heroHealth.DamageHeathBy(enemyAttackSystem.enemyDamage);
            }
            enemyAttackSystem.waitForExplosion = false;
        }
        if(explosiveParticle != null)
        {
            explosiveParticle.transform.position = redZone.transform.position;
            explosiveParticle.Play();
        }
        
    }
}
