﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimationEventsManager : MonoBehaviour
{

    public Action actionEnemyHitHero;
    public Action actionHeroHitEnemy;
    public Action actionEnemyAttackAnimEnd;
    public Action actionEnemyThrowThrowable;
    public Action actionHeroSuperMove;
    public Action actionHeroSuperMoveDisable;
    public Action actionEnemyThrowWeapon;
    public Action actionHeroAttackEnd;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EnemyHitHero()
    {
        actionEnemyHitHero?.Invoke();
        //Debug.Log("Hit");
    }
    public void EnemyThrowThrowable()
    {
        actionEnemyThrowThrowable?.Invoke();
        //Debug.Log("Hit");
    }
    public void HeroHitEnemy()
    {
        actionHeroHitEnemy?.Invoke();
    }
    public void EnemyAttackEnd()
    {
        actionEnemyAttackAnimEnd?.Invoke();
        //Debug.Log("End");
    }
    public void HeroSuperMoveAnimEnvent()
    {
        actionHeroSuperMove.Invoke();
    }
    public void HeroAttackEndAnimEnvent()
    {
        actionHeroAttackEnd.Invoke();
    }
    public void HeroSuperMoveDisableAnimEnvent()
    {
        actionHeroSuperMoveDisable.Invoke();
    }
    public void EnemyThrowWeaponAnimEvent()
    {
        actionEnemyThrowWeapon.Invoke();
    }
}
