﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using APSdk;

public class GameStatesControl : MonoBehaviour
{
    [SerializeField]
    private GameStates gameStateCurrent;

    public Action actionMenuIdle    ;
    public Action actionGameStart   ;
    public Action actionLevelStart  ;
    public Action actionGamePlay    ;
    public Action actionLevelResult ;
    public Action actionLevelEnd    ;


    public static GameStatesControl instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        AssignFuntionsToActions();
        ChangeGameStateTo(GameStates.MenuIdle);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeGameStateTo(GameStates state , float t)
    {
        StartCoroutine(ChangeGameStateCoRoutine(state,  t));
    }
    public GameStates GetCurrentGameState()
    {
        return gameStateCurrent;
    }
    public IEnumerator ChangeGameStateCoRoutine(GameStates state , float t)
    {
        yield return new WaitForSeconds(t);
        ChangeGameStateTo(state);
    }
    public void ChangeGameStateTo(GameStates state)
    {
        Debug.Log("Changed State To: " + state);
        if (gameStateCurrent == state)
        {
            //return;
        }
        gameStateCurrent = state;

        switch (state)
        {
            case GameStates.MenuIdle   :
                {
                    
                    actionMenuIdle   ?.Invoke();
                    
                    break;
                }
            case GameStates.GameStart  :
                {
                    actionGameStart  ?.Invoke();

                    ChangeGameStateTo(GameStates.LevelStart);

                    break;
                }
            case GameStates.LevelStart :
                {
                    
                    ChangeGameStateTo(GameStates.GamePlay ,0.5f );
                    
                    actionLevelStart ?.Invoke();
                    
                    break;
                }
            case GameStates.GamePlay   :
                {
                    actionGamePlay   ?.Invoke();
                    break;
                }
            case GameStates.LevelResult:
                {
                    actionLevelResult?.Invoke();


                    if (LevelManager.instance.levelResultType == LevelResultType.Success)
                    {
                        APLionKitWrapper.Instance.InterstitialAd.ShowInterstitialAd(InterStitialAdCloseFun , InterStitialAdCloseFun);
                    }
                    else
                    {
                        InterStitialAdCloseFun();
                    }
                        

                    
                    break;
                }
            case GameStates.LevelEnd:
                {
                    actionLevelEnd?.Invoke();
                    //ReferenceMaster.instance.superMoveUnlockManager.SuperMoveUnlockLeveleEndFun();
                    if(LevelManager.instance.levelResultType == LevelResultType.Success)
                    {
                        SuperPowersUnlockSystem.instance.SuperMoveUiAppearOrNot();
                    }
                    else
                    {
                        LevelFailedCountdownSystem.instance.StartRevive();
                    }
                    
                    break;
                }
        }
    }

    public void AssignFuntionsToActions()
    {
        actionMenuIdle += UiManage.instance.MenuIdlePanelAppear;
        actionMenuIdle += UiManage.instance.UiValuesUpdate;
        actionMenuIdle += InputTouchModeControl.instance.DisableTouchThroughTouchMode;
        //actionMenuIdle += PanelSavedProgress.instance.DecideAppearOrNotSavedProgressPanel;

        actionMenuIdle += AreaSelectionUiManage.instance.DetermineAreaAtMenuUiState;
        GameStatesControl.instance.actionMenuIdle +=AttentionIndicatorControl.instance.CheckIndicator;
        GameStatesControl.instance.actionMenuIdle += ReferenceMaster.instance.heroHealth.GetComponent<HeroMover>().HeroRepositionToCenter;
        //GameStatesControl.instance.actionMenuIdle += LevelManager.instance.GetComponent<LevelNumberRandomizer>().ShuffleLevels;
        GameStatesControl.instance.actionMenuIdle += UiMouseInputs.instance.TouchDisable;
        
        //actionMenuIdle += ReferenceMaster.instance.superMoveUnlockManager.DetectUnlockAtMenuIdle;
        //actionMenuIdle += LevelManager.instance.MenuIdleFun;


        //
        actionGameStart += UiManage.instance.GamePlayPanelAppear;
        actionGameStart += UiManage.instance.UiValuesUpdate;
        GameStatesControl.instance.actionGameStart += UiMouseInputs.instance.TouchDisable;
        //actionGameStart += LevelManager.instance.GameStartFun;
        //actionGameStart += LevelValuesAssignManager.instance.LevelDataTransferAtGameStart;
        //
        actionLevelStart += LevelManager.instance.LevelStartFun;
        actionLevelStart += InputTouchModeControl.instance.EnableTouchThroughTouchMode;
        actionLevelStart +=  GameManagementMain.instance.AnalyticsCallLevelStarted ;
        //
        actionLevelResult += UiManage.instance.LevelResultPanelAppear;
        //actionLevelResult += LevelManager.instance.LevelResultFunction;
        //actionLevelResult += InputMouseSystem.instance.TouchDisable;
        //
        actionLevelEnd += UiManage.instance.LevelEndPanelAppear;
        actionLevelEnd += InputTouchModeControl.instance.DisableTouchThroughTouchMode;
    }

    void InterStitialAdCloseFun()
    {
        ChangeGameStateTo(GameStates.LevelEnd, 1);
    }
}
