﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UiMouseInputs : InputMouseSystem
{
    public static UiMouseInputs insatnce;
    private void Awake()
    {
        insatnce = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //actionTouchIn += TestFunUi;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!touchEnabled)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            actionTouchIn?.Invoke();
            swipping = true;
            oldPos = newPos = Input.mousePosition;

            mouseProgression = newPos - oldPos;

            mouseProgressionLastFrame = mouseProgression;
        }
        if (Input.GetMouseButton(0))
        {
            tapped = true;
            //oldPos = newPos;
            newPos = Input.mousePosition;
            mouseProgression = newPos - oldPos;
            mouseVelocity = (mouseProgression - mouseProgressionLastFrame) / Screen.width;
            mouseProgressionLastFrame = mouseProgression;

            actionTouchHolded?.Invoke();

            if (swipping && Mathf.Abs(mouseProgression.x) > (Screen.width * swipeThreshold))
            {
                if (mouseProgression.x > 0)
                {
                    actionSwipeRight?.Invoke();
                }
                else
                {
                    actionSwipeLeft?.Invoke();
                }

                swipping = false;
            }
        }
        else
        {
            tapped = false;
            swipping = false;
        }
        if (Input.GetMouseButtonUp(0))
        {
            actionTouchOut?.Invoke();
            oldPos = newPos;
            mouseVelocity = Vector2.zero;
        }

        mouseProgressionWRTScreen = mouseProgression / Screen.width;
    }
    public void TestFunUi()
    {
        Debug.Log("UI Clicked");
    }
}
