﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelNumberRandomizer : MonoBehaviour
{
    public int loadedShuffledLevelIndexCurrent;
    public int levelBlockSize;
    public int levelBlocks;
    List<int> l;
    public List<int> shuffledAllLevelIndexes;
    public bool shuffleLevels;
    // Start is called before the first frame update
    void Start()
    {
        ShuffleLevels();
        GameStatesControl.instance.actionLevelStart += LevelManager.instance.GetComponent<LevelNumberRandomizer>().ShuffleLevelsAtGameStart;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ShuffleLevels()
    {
        shuffledAllLevelIndexes.Clear();
        //List<int> shuffledAllLvls= new List<int>();
        for (int i = 0; i < levelBlocks; i++)
        {
            List<int> s = ShuffleABlock();
            for (int j = 0; j < s.Count; j++)
            {
                shuffledAllLevelIndexes.Add((i * levelBlockSize) + s[j]);
            }
        }

        //HardCode
        HardCodedCorrectionForSuperTut();
    }
    public List<int> ShuffleABlock()
    {
        List<int> nonShuffledBlock = new List<int>();
        List<int> shuffledBlock = new List<int>();
        for (int i = 0; i < levelBlockSize; i++)
        {
            nonShuffledBlock.Add(i);
        }
        if (!shuffleLevels)
        {
            return nonShuffledBlock;
        }
        while (nonShuffledBlock.Count > 0)
        {
            int n = Random.Range(0, nonShuffledBlock.Count);
            shuffledBlock.Add(nonShuffledBlock[n]);
            nonShuffledBlock.RemoveAt(n);
        }
        l = shuffledBlock;

        return shuffledBlock;
    }
    public int GetCurrentShuffleLevelOf(int l)
    {
        loadedShuffledLevelIndexCurrent = shuffledAllLevelIndexes[l];
        return shuffledAllLevelIndexes[l];
    }

    public void HardCodedCorrectionForSuperTut()
    {
        int l = SuperPowersUnlockSystem.instance.superMovesAvailable[0].unlockAtLevelIndex + 1;
        int index = shuffledAllLevelIndexes.IndexOf(l);
        int curVal = shuffledAllLevelIndexes[l];
        shuffledAllLevelIndexes[index] = curVal;
        shuffledAllLevelIndexes[l] = l;

        //Debug.Log("ind " + index);
    }
    public void ShuffleLevelsAtGameStart()
    {
        if(GameManagementMain.instance.levelIndex == 0)
        {
            ShuffleLevels();
        }
    }
}
