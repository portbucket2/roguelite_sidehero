﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{
    public Animator myAnim;
    public Collider[] ragdollColliders;
    public Rigidbody[] ragdollrbs;
    public Rigidbody ragHitRb;
    public PhysicMaterial lessFricMat;
    // Start is called before the first frame update
    void Start()
    {
        ragdollColliders = GetComponentsInChildren<Collider>();
        ragdollrbs = GetComponentsInChildren<Rigidbody>();
        myAnim = GetComponent<Animator>();
        DisableRagdoll();

        for (int i = 0; i < ragdollColliders.Length; i++)
        {
            ragdollColliders[i].material = lessFricMat;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EnableRagdoll()
    {
        myAnim.enabled = false;
        for (int i = 0; i < ragdollColliders.Length; i++)
        {
            ragdollColliders[i].enabled = true;
        }
        for (int i = 0; i < ragdollrbs. Length; i++)
        {
            ragdollrbs[i].useGravity = true;
        }
    }
    public void DisableRagdoll()
    {
        myAnim.enabled = true;
        for (int i = 0; i < ragdollColliders.Length; i++)
        {
            ragdollColliders[i].enabled = false;
        }
        for (int i = 0; i < ragdollrbs.Length; i++)
        {
            ragdollrbs[i].useGravity = false;
        }
    }
    public Rigidbody GetRagHitRb()
    {
        return ragHitRb;
    }
}
