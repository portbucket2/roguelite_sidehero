﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackSystem : MonoBehaviour
{
    public EnemyType enemyType;
    public float attackInterval;
    public float attackIntervalApp
    {
        get
        {
            if (AbilityCardMultipliers.instance != null)
            {
                return ((float)attackInterval * AbilityCardMultipliers.instance.enemyAttackIntervalMul);
            }
            else
            {
                return attackInterval;
            }

        }
    }

    public float initialDelay;
    public float enemyAttackRange;
    public ParticleSystem enemyAttackParticle;
    float attackTimer;
    float initialDealyTimer;
    public bool attackLoopRunning;
    bool gotPlayerFirstTime;
    public HeroHealth heroHealthInRange;
    EnemyHealth enemyHealth;
    public bool attackAnimPlaying;
    public float enemyDamage;
    public bool waitForExplosion;

    public GameObject hammerRedZone;
    public ParticleSystem hammerHitParticle;
    // Start is called before the first frame update
    void Start()
    {
        heroHealthInRange = ReferenceMaster.instance.heroHealth;
        enemyHealth = GetComponent<EnemyHealth>();

        GetComponentInChildren<AnimationEventsManager>().actionEnemyHitHero += EnemyAttackHit;
        GetComponentInChildren<AnimationEventsManager>().actionEnemyAttackAnimEnd += AttackAnimEnd;

        //if(enemyType == EnemyType.Grenade)
        //{
        //    //GetComponentInChildren<AnimationEventsManager>().actionEnemyThrowThrowable += GetComponentInChildren<ExplosiveThrowable>().ThrowIt;
        //}
        //if (enemyType == EnemyType.Molotov)
        //{
        //    //GetComponentInChildren<AnimationEventsManager>().actionEnemyThrowThrowable += GetComponentInChildren<ExplosiveThrowable>().ThrowIt;
        //}
        if (hammerRedZone != null)
        {
            hammerRedZone.transform.localScale = new Vector3(1, 1, enemyAttackRange - 0.5f);
            hammerRedZone.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHealth.isDead) { return; }
        RunAttackLoop();
    }
    public void RunAttackLoop()
    {
        if (!attackLoopRunning)
        {
            return;
        }
        if (attackAnimPlaying)
        {
            return;
        }
        if (!gotPlayerFirstTime)
        {
            return;
        }
        if (waitForExplosion)
        {
            return;
        }
        if(attackTimer == 0)
        {
            EnemyAttack();
        }
        attackTimer += Time.deltaTime;
        if(attackTimer >= attackIntervalApp)
        {
            
            attackTimer = 0;
            
        }
    }
    void EnemyAttack()
    {
        if (attackAnimPlaying)
        {
            return;
        }
        enemyAttackParticle.Play();
        GetComponent<EnemyAnimControl>().EnemyAttackAnimPlay();
        AttackAnimStart();

        switch (enemyType)  /// THIS SWITCH CASE CAN BE CONVERTED TO ENEMYCATEGORY
        {
            case EnemyType.Ordinary:
                {

                    break;
                }
            case EnemyType.Hammer:
                {
                    hammerRedZone.SetActive(true);
                    
                    break;
                }
            case EnemyType.Molotov:
                {
                    GetComponentInChildren<ExplosiveThrowable>().RedZoneIndicate(enemyAttackRange);
                    break;
                }
            case EnemyType.Grenade:
                {
                    GetComponentInChildren<ExplosiveThrowable>().RedZoneIndicate(enemyAttackRange);
                    break;
                }
            case EnemyType.Suriken:
                {
                    //GetComponentInChildren<WeaponThrower>().ThrowWeapon();
                    break;
                }
            case EnemyType.Machette:
                {
                    
                    break;
                }
            case EnemyType.Dagger:
                {
                    
                    break;
                }
            case EnemyType.Baseball:
                {
                    hammerRedZone.SetActive(true);
                    break;
                }
            case EnemyType.Crowbar:
                {
                    hammerRedZone.SetActive(true);
                    break;
                }
            case EnemyType.PickAxe:
                {
                    hammerRedZone.SetActive(true);
                    break;
                }
            case EnemyType.Knife:
                {
                    
                    break;
                }
        }
        //StartCoroutine(EnemyAttackHitCoroutine(0.2f));

    }
    //IEnumerator EnemyAttackHitCoroutine(float d = 0)
    //{
    //    yield return new WaitForSeconds(d);
    //    EnemyAttackHit();
    //}
    public void EnemyAttackHit()
    {
        if(Vector3.Distance(transform.position, heroHealthInRange.transform.position) <= enemyAttackRange)
        {
            heroHealthInRange.DamageHeathBy(enemyDamage);
        }
        switch (enemyType)
        {
            case EnemyType.Ordinary:
                {

                    break;
                }
            case EnemyType.Hammer:
                {
                    hammerRedZone.SetActive(false);
                    hammerHitParticle.Play();
                    break;
                }
            case EnemyType.Molotov:
                {
                    
                    break;
                }
            case EnemyType.Grenade:
                {
                    
                    break;
                }
            case EnemyType.Suriken:
                {
                    
                    break;
                }
            case EnemyType.Machette:
                {

                    break;
                }
            case EnemyType.Dagger:
                {

                    break;
                }
            case EnemyType.Baseball:
                {
                    hammerRedZone.SetActive(false);
                    break;
                }
            case EnemyType.Crowbar:
                {
                    hammerRedZone.SetActive(false);
                    break;
                }
            case EnemyType.PickAxe:
                {
                    hammerRedZone.SetActive(false);
                    break;
                }
            case EnemyType.Knife:
                {

                    break;
                }
        }

    }
    public void EnableAttackLoop()
    {
        if (attackLoopRunning)
        {
            return;
        }
        if (ReferenceMaster.instance.heroHealth.HeroDeadOrnot())
        {
            return;
        }
        attackLoopRunning = true;
        if (!gotPlayerFirstTime)
        {
            StartCoroutine(InitialDelayThenAttack());
        }
    }
    public void PauseAttackLoop()
    {
        attackLoopRunning = false;
    }
    IEnumerator InitialDelayThenAttack()
    {
        AttentionIndicatorControl.instance.CheckIndicator();
        yield return new WaitForSeconds(initialDelay);
        //EnemyAttack();
        gotPlayerFirstTime = true;
    }
    public void AttackAnimEnd()
    {
        attackAnimPlaying = false;
        //EnableAttackLoop();
    }
    public void AttackAnimStart()
    {
        attackAnimPlaying = true;
    }
}
