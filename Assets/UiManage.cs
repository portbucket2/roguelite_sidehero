﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UiManage : MonoBehaviour
{
    public GameObject panelMenuIdle;
    public GameObject panelLevelResult;
    public GameObject panelLevelEnd;
    public GameObject panelGamePlay;
    public GameObject panelBossFight;

    public GameObject levelResultSuccess;
    public GameObject levelResultFail;
    public GameObject levelEndSuccess;
    public GameObject levelEndFail;

    public static UiManage instance;
    public Text[] levelTexts;

    public Action actionUiDelayed;
    public Action actionUiManageGameStartButton;
    public Button buttonStart;
    public Slider sliderLevelCompletion;
    public float levelCompletonValue;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        EnemySpawner.instance.actionEnemyDeath += UiRuntimeValuesUpdate;
        LevelManager.instance.actionLevelLoadingDone += UiRuntimeValuesUpdate;
    }

    // Update is called once per frame
    //void Update()
    //{
    //    
    //}

    public void MenuIdlePanelAppear()
    {
        panelMenuIdle    .SetActive(true);
        panelLevelResult .SetActive(false);
        panelLevelEnd    .SetActive(false);
        panelGamePlay    .SetActive(false);
    }
    public void GamePlayPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelLevelResult.SetActive(false);
        panelLevelEnd.SetActive(false);
        panelGamePlay.SetActive(true);
    }
    public void LevelResultPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelLevelResult.SetActive(true);
        panelLevelEnd.SetActive(false);
        panelGamePlay.SetActive(false);
    }
    public void LevelEndPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelLevelResult.SetActive(false);
        panelLevelEnd.SetActive(true);
        panelGamePlay.SetActive(false);
    }

    public void UiSuccessPanelsAppear()
    {
        levelResultSuccess.SetActive(true);
        levelResultFail   .SetActive(false);
        levelEndSuccess   .SetActive(true);
        levelEndFail      .SetActive(false);
    }
    public void UiFailedPanelsAppear()
    {
        levelResultSuccess.SetActive(false);
        levelResultFail.SetActive(true);
        levelEndSuccess.SetActive(false);
        levelEndFail.SetActive(true);
    }
    public void StartButtonFun()
    {
        actionUiManageGameStartButton?.Invoke();
        GameStatesControl.instance.ChangeGameStateTo(GameStates.GameStart);
    }
    public void RestartButtonFun()
    {
        //Application.LoadLevel(0);
        LevelManager.instance.LevelReset();
        GameStatesControl.instance.ChangeGameStateTo(GameStates.MenuIdle);
        //buttonStart.gameObject.SetActive(false);
        
    }
    public void NextButtonFun()
    {
        //if (!AreaSelectionUiManage.instance.EvaluateLastLevelOfThisAreaOrNot())
        //{
        //    GameManagementMain.instance.NextLevelUpdate();
        //    RestartButtonFun();
        //    GameStatesControl.instance.ChangeGameStateTo(GameStates.GameStart);
        //}
        //else
        //{
        //    GameManagementMain.instance.NextLevelUpdate();
        //    RestartButtonFun();
        //    
        //}
        GameManagementMain.instance.NextLevelUpdate();
        RestartButtonFun();
        GameStatesControl.instance.ChangeGameStateTo(GameStates.GameStart);
    }
    
    public void GotItButtonFun()
    {
        LevelEndCardsUi.instance.AppearCardsUi();
    }
    public void UiValuesUpdate()
    {
        for (int i = 0; i < levelTexts.Length; i++)
        {
            levelTexts[i].text = "LEVEL " + (GameManagementMain.instance.levelIndexDisplayed + 1);
        }
        
    }
    public void UiRuntimeValuesUpdate()
    {
        sliderLevelCompletion.value = SliderFillBy();
    }
    public float SliderFillBy()
    {
        float totalEnemies = LevelValuesCurrent.instance.totalEnemiesCountInCurrentLevel;
        float remainingEnemies = LevelValuesCurrent.instance.enemyListCurrentLevel.Count + LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count;
        float val = 0;
        if (LevelValuesCurrent.instance.hasBossFight)
        {
            remainingEnemies = LevelValuesCurrent.instance.enemyListCurrentLevel.Count + LevelValuesCurrent.instance.enemyAliveCurrentLevel.Count +
                LevelValuesCurrent.instance.bossListCurrentLevel.Count + LevelValuesCurrent.instance.bossAliveCurrentLevel.Count;
        }
        if (totalEnemies > 0)
        {
            val = (1 - remainingEnemies / totalEnemies);
        }
        levelCompletonValue = val;
        return val;
    }
    public float GetLevelCompletionValue()
    {
        
        return sliderLevelCompletion.value;
    }
    public void BossFightPanelAppear()
    {
        panelBossFight.SetActive(true);
    }
    public void BossFightPanelDisAppear()
    {
        panelBossFight.SetActive(false);
    }

    public void DelayedUiAppear(float t)
    {
        StartCoroutine(delayedUiCoroutine(t));
    }
    IEnumerator delayedUiCoroutine(float t)
    {
        yield return new WaitForSeconds(t);
        actionUiDelayed?.Invoke();
        actionUiDelayed = null;
    }
}
